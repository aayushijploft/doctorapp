package com.telemedicine.doctorapp.component.auth.activity

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.telemedicine.doctorapp.R
import com.telemedicine.doctorapp.component.auth.viewmodel.LoginViewModel
import com.telemedicine.doctorapp.component.home.activity.MainActivity
import com.telemedicine.doctorapp.component.home.viewmodel.MainViewModel
import com.telemedicine.doctorapp.databinding.ActivityLoginBinding
import com.telemedicine.doctorapp.databinding.ActivityMainBinding
import com.telemedicine.doctorapp.network.Apis
import com.telemedicine.doctorapp.pojo.login.LoginData
import com.telemedicine.doctorapp.pojo.login.LoginResponse
import com.telemedicine.doctorapp.utils.Utility
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity() {
    private lateinit var viewModel: LoginViewModel

    private var binding: ActivityLoginBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, LoginViewModel.Factory(this))
            .get(LoginViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding!!.setLifecycleOwner(this)
        binding!!.viewModel = viewModel
        binding!!.tvLogin.setOnClickListener {
            val loginData = LoginData()
            loginData.phone = binding!!.etMobile.text.toString()
            loginData.password = binding!!.etPassword.text.toString()
            Login(loginData)
        }
    }

    fun Login(loginData: LoginData) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_login)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.show()
        val call: Call<LoginResponse?>? = Apis.aPIService.login(loginData)
        call!!.enqueue(object : Callback<LoginResponse?> {
            override fun onResponse(
                call: Call<LoginResponse?>,
                response: Response<LoginResponse?>
            ) {
                dialog.dismiss()
                val user: LoginResponse? = response.body()
                if(response.code() == 500){
                    Toast.makeText(this@LoginActivity,"Incorrect username or password.",Toast.LENGTH_SHORT).show()
                }
                else{
                    if (user!!.status!!.equals("true")){
                        Log.e("userId",user.data!!.userId!!.toString())
                        Log.e("userToken",user.data!!.userToken!!.toString())
                        Utility.setloginID(user.data!!.userId!!.toString())
                        Utility.setUserToken(user.data!!.userToken!!.toString())
                        startActivity(Intent(this@LoginActivity,MainActivity::class.java))
                    }
                }
            }

            override fun onFailure(
                call: Call<LoginResponse?>,
                t: Throwable
            ) {
                dialog.dismiss()
                Log.d("response", "gh" + t.message)
            }

        })
    }
}