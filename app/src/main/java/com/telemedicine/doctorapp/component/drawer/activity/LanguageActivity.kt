package com.telemedicine.doctorapp.component.drawer.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.telemedicine.doctorapp.R
import com.telemedicine.doctorapp.component.drawer.viewmodel.LanguageViewModel
import com.telemedicine.doctorapp.component.home.viewmodel.MainViewModel
import com.telemedicine.doctorapp.databinding.ActivityLanguageBinding
import com.telemedicine.doctorapp.databinding.ActivityMainBinding

class LanguageActivity : AppCompatActivity() {
    private lateinit var viewModel: LanguageViewModel

    private var binding: ActivityLanguageBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, LanguageViewModel.Factory(this))
            .get(LanguageViewModel::class.java)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_language)

        binding!!.setLifecycleOwner(this)

        binding!!.viewModel = viewModel

        binding!!.ivBack.setOnClickListener { onBackPressed() }

    }
}