package com.telemedicine.doctorapp.component.drawer.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.tabs.TabLayout
import com.telemedicine.doctorapp.R
import com.telemedicine.doctorapp.component.drawer.fragments.ChangePasswordFragment
import com.telemedicine.doctorapp.component.drawer.fragments.DigitalSignatureFragment
import com.telemedicine.doctorapp.component.drawer.viewmodel.SettingViewModel
import com.telemedicine.doctorapp.databinding.ActivitySettingBinding

class SettingActivity : AppCompatActivity() {
    private lateinit var viewModel: SettingViewModel

    private var binding: ActivitySettingBinding? = null
    internal lateinit var viewpageradapter: ViewPagerAdapter //Declare PagerAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, SettingViewModel.Factory(this))
            .get(SettingViewModel::class.java)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_setting)

        binding!!.setLifecycleOwner(this)


        binding!!.viewModel = viewModel
        binding!!.ivBack.setOnClickListener { onBackPressed() }

        viewpageradapter=
            ViewPagerAdapter(supportFragmentManager)

        binding!!.viewPager.adapter=viewpageradapter  //Binding PagerAdapter with ViewPager
        binding!!.tabLayout.setupWithViewPager(binding!!.viewPager) //Binding ViewP

        binding!!.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                if(tab.position == 1){
                    binding!!.tvPreview.visibility = View.VISIBLE
                }
                else binding!!.tvPreview.visibility = View.INVISIBLE
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {

            }
            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

    }
    internal  class ViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        var fragment: Fragment? = null

        override fun getItem(position: Int): Fragment {
            if (position == 0) {
                fragment = ChangePasswordFragment()
            } else if (position == 1) {
                fragment = DigitalSignatureFragment()
            }
            return fragment!!
        }

        override fun getCount(): Int {
            return 2
        }


        override fun getPageTitle(position: Int): CharSequence? {
            var title: String? = null
            if (position == 0) {
                title = "CHANGE PASSWORD"
            } else if (position == 1) {
                title = "Digital Signature"
            }
            return title
        }
    }


}