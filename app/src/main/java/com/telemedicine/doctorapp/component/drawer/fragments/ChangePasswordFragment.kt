package com.telemedicine.doctorapp.component.drawer.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.telemedicine.doctorapp.R
import com.telemedicine.doctorapp.component.drawer.viewmodel.ChangePasswordViewModel
import com.telemedicine.doctorapp.component.home.fragments.appointment.subfragments.past.PastViewModel
import com.telemedicine.doctorapp.databinding.FragmentChangePasswordBinding
import com.telemedicine.doctorapp.databinding.FragmentPastBinding


class ChangePasswordFragment : Fragment() {

    private lateinit var viewModel: ChangePasswordViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        viewModel = ViewModelProvider(this, ChangePasswordViewModel.Factory(requireActivity()))
            .get(ChangePasswordViewModel::class.java)
        val binding: FragmentChangePasswordBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_change_password, container, false)
        binding.viewModel = viewModel


        return binding.root
    }

}