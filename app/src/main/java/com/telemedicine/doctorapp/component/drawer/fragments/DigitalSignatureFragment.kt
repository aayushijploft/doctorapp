package com.telemedicine.doctorapp.component.drawer.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.telemedicine.doctorapp.R
import com.telemedicine.doctorapp.component.drawer.viewmodel.ChangePasswordViewModel
import com.telemedicine.doctorapp.component.drawer.viewmodel.DigitalSignatureViewModel
import com.telemedicine.doctorapp.databinding.FragmentChangePasswordBinding
import com.telemedicine.doctorapp.databinding.FragmentDigitalSignatureBinding

class DigitalSignatureFragment : Fragment() {

    private lateinit var viewModel: DigitalSignatureViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        viewModel = ViewModelProvider(this, DigitalSignatureViewModel.Factory(requireActivity()))
            .get(DigitalSignatureViewModel::class.java)
        val binding: FragmentDigitalSignatureBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_digital_signature, container, false)
        binding.viewModel = viewModel


        return binding.root
    }

}