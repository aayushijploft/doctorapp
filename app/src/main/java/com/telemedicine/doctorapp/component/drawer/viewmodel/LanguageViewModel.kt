package com.telemedicine.doctorapp.component.drawer.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class LanguageViewModel(val context: Context) : ViewModel(){



    class Factory(val context: Context) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {

            return LanguageViewModel(
                context
            ) as T
        }
    }

}