package com.telemedicine.doctorapp.component.home.activity

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.DatePicker
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.jploft.dramaking.ui.base.BaseBindingActivity
import com.telemedicine.doctorapp.R
import com.telemedicine.doctorapp.component.drawer.activity.LanguageActivity
import com.telemedicine.doctorapp.component.drawer.activity.SettingActivity
import com.telemedicine.doctorapp.component.home.fragments.appointment.appointmentFragment
import com.telemedicine.doctorapp.component.home.fragments.createschedule.CreateScheduleFragment
import com.telemedicine.doctorapp.component.home.fragments.invites.InviteFragment
import com.telemedicine.doctorapp.component.home.fragments.uploads.UploadFragment
import com.telemedicine.doctorapp.component.home.viewmodel.MainViewModel
import com.telemedicine.doctorapp.databinding.ActivityMainBinding
import com.telemedicine.doctorapp.interfaces.DrawerItemClick
import com.telemedicine.doctorapp.network.Apis
import com.telemedicine.doctorapp.pojo.createschedule.schedules.CreateScheduleData
import com.telemedicine.doctorapp.pojo.createschedule.schedules.CreateScheduleResponse
import com.telemedicine.doctorapp.utils.Utility
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : BaseBindingActivity(), DrawerItemClick {

    private lateinit var viewModel: MainViewModel

    private var binding: ActivityMainBinding? = null

    var value : String = ""

    override fun createActivityObject(savedInstanceState: Bundle?) {
        mActivity = this
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, MainViewModel.Factory(this))
            .get(MainViewModel::class.java)

        binding = DataBindingUtil.setContentView(this@MainActivity, R.layout.activity_main)

        binding!!.setLifecycleOwner(this)

        binding!!.viewModel = viewModel

        if(intent.hasExtra("value")){
            value = intent.getStringExtra("value").toString()
            Log.e("value",value)
        }
        val firstFragment=appointmentFragment()
        val secondFragment=CreateScheduleFragment()
        val thirdFragment=UploadFragment()
        val fourthFragment=InviteFragment()

        if(value.equals("createschedule")){
            binding!!.tvTitle.text = "Create Schedule"
            binding!!.ivCreateSchedule.visibility = View.VISIBLE
            replaceFragment(secondFragment, null)
            binding!!.ivAppointment.setColorFilter(
                ContextCompat.getColor(
                    this,
                    R.color.darkgreay
                )
            )
            binding!!.ivCreate.setColorFilter(ContextCompat.getColor(this, R.color.green))
            binding!!.ivUpload.setColorFilter(ContextCompat.getColor(this, R.color.darkgreay))
            binding!!.ivInvite.setColorFilter(ContextCompat.getColor(this, R.color.darkgreay))

            binding!!.tvAppointment.setTextColor(resources.getColor(R.color.darkgreay))
            binding!!.tvCreate.setTextColor(resources.getColor(R.color.green))
            binding!!. tvUpload.setTextColor(resources.getColor(R.color.darkgreay))
            binding!!.tvInvite.setTextColor(resources.getColor(R.color.darkgreay))
        }
        else{
            replaceFragment(firstFragment, null)
            binding!!.tvTitle.text = "Appointments"
            binding!!.ivAppointment.setColorFilter(ContextCompat.getColor(this, R.color.green))
            binding!!.ivCreate.setColorFilter(ContextCompat.getColor(this, R.color.darkgreay))
            binding!!.ivUpload.setColorFilter(ContextCompat.getColor(this, R.color.darkgreay))
            binding!!.ivInvite.setColorFilter(ContextCompat.getColor(this, R.color.darkgreay))

            binding!!.tvAppointment.setTextColor(resources.getColor(R.color.green))
            binding!!.tvCreate.setTextColor(resources.getColor(R.color.darkgreay))
            binding!!.tvUpload.setTextColor(resources.getColor(R.color.darkgreay))
            binding!!. tvInvite.setTextColor(resources.getColor(R.color.darkgreay))
        }

        binding!!.ivMenu.setOnClickListener {
            openDrawer()
        }

        binding!!.ivCreateSchedule.setOnClickListener {
            showCreateScheduleDialog()
        }

        binding!!.layoutAppointment!!.setOnClickListener {
            binding!!.tvTitle!!.text = "Appointments"
            binding!!.ivCreateSchedule!!.visibility = View.INVISIBLE
            replaceFragment(firstFragment, null)
            binding!!.ivAppointment!!.setColorFilter(ContextCompat.getColor(this, R.color.green))
            binding!!.ivCreate!!.setColorFilter(ContextCompat.getColor(this, R.color.darkgreay))
            binding!!.ivUpload!!.setColorFilter(ContextCompat.getColor(this, R.color.darkgreay))
            binding!!.ivInvite!!.setColorFilter(ContextCompat.getColor(this, R.color.darkgreay))

            binding!!.tvAppointment!!.setTextColor(resources.getColor(R.color.green))
            binding!!.tvCreate!!.setTextColor(resources.getColor(R.color.darkgreay))
            binding!!.tvUpload!!.setTextColor(resources.getColor(R.color.darkgreay))
            binding!!. tvInvite!!.setTextColor(resources.getColor(R.color.darkgreay))
        }

        binding!!.layoutCreate!!.setOnClickListener {
            binding!!.tvTitle!!.text = "Create Schedule"
            binding!!.ivCreateSchedule!!.visibility = View.VISIBLE
            replaceFragment(secondFragment, null)
            binding!!.ivAppointment!!.setColorFilter(
                ContextCompat.getColor(
                    this,
                    R.color.darkgreay
                )
            )
            binding!!.ivCreate!!.setColorFilter(ContextCompat.getColor(this, R.color.green))
            binding!!.ivUpload!!.setColorFilter(ContextCompat.getColor(this, R.color.darkgreay))
            binding!!.ivInvite!!.setColorFilter(ContextCompat.getColor(this, R.color.darkgreay))

            binding!!.tvAppointment!!.setTextColor(resources.getColor(R.color.darkgreay))
            binding!!.tvCreate!!.setTextColor(resources.getColor(R.color.green))
            binding!!. tvUpload!!.setTextColor(resources.getColor(R.color.darkgreay))
            binding!!.tvInvite!!.setTextColor(resources.getColor(R.color.darkgreay))
        }

        binding!!.layoutUpload!!.setOnClickListener {
            binding!!.tvTitle!!.text = "Waiting Room Uploads"
            binding!!.ivCreateSchedule!!.visibility = View.INVISIBLE
            replaceFragment(thirdFragment, null)
            binding!!.ivAppointment!!.setColorFilter(
                ContextCompat.getColor(
                    this,
                    R.color.darkgreay
                )
            )
            binding!!.ivCreate.setColorFilter(ContextCompat.getColor(this, R.color.darkgreay))
            binding!!.ivUpload.setColorFilter(ContextCompat.getColor(this, R.color.green))
            binding!!.ivInvite.setColorFilter(ContextCompat.getColor(this, R.color.darkgreay))

            binding!!.tvAppointment.setTextColor(resources.getColor(R.color.darkgreay))
            binding!!.tvCreate.setTextColor(resources.getColor(R.color.darkgreay))
            binding!!.tvUpload.setTextColor(resources.getColor(R.color.green))
            binding!!.tvInvite.setTextColor(resources.getColor(R.color.darkgreay))
        }

        binding!!.layoutInvite!!.setOnClickListener {
            binding!!.tvTitle!!.text = "Invites"
            binding!!.ivCreateSchedule!!.visibility = View.INVISIBLE
            replaceFragment(fourthFragment, null)
            binding!!.ivAppointment!!.setColorFilter(
                ContextCompat.getColor(
                    this,
                    R.color.darkgreay
                )
            )
            binding!!.ivCreate!!.setColorFilter(ContextCompat.getColor(this, R.color.darkgreay))
            binding!!.ivUpload!!.setColorFilter(ContextCompat.getColor(this, R.color.darkgreay))
            binding!!.ivInvite!!.setColorFilter(ContextCompat.getColor(this, R.color.green))

            binding!!.tvAppointment!!.setTextColor(resources.getColor(R.color.darkgreay))
            binding!!.tvCreate!!.setTextColor(resources.getColor(R.color.darkgreay))
            binding!!.tvUpload!!.setTextColor(resources.getColor(R.color.darkgreay))
            binding!!.tvInvite!!.setTextColor(resources.getColor(R.color.green))
        }
    }


    fun closeDrawer() {
        if (binding!!.drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            binding!!.drawerLayout.closeDrawer(Gravity.LEFT)
        }
    }

    fun openDrawer() {
        if (!binding!!.drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            binding!!.drawerLayout.openDrawer(Gravity.LEFT)
        }
    }

    private fun showCreateScheduleDialog() {
        var matchDialog: Dialog? = null
        matchDialog = Dialog(this)
        matchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        matchDialog.setCancelable(false)
        matchDialog.setContentView(R.layout.layout_create_schedule)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(matchDialog.getWindow()!!.getAttributes())
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        matchDialog.getWindow()!!.setAttributes(lp)

        val tvFrom = matchDialog.findViewById<TextView>(R.id.tvFrom)
        val tvTo = matchDialog.findViewById<TextView>(R.id.tvTo)
        val tvSave = matchDialog.findViewById<TextView>(R.id.tvSave)
        val tvCancel = matchDialog.findViewById<TextView>(R.id.tvCancel)
        val tvSun = matchDialog.findViewById<TextView>(R.id.tvSun)
        val tvMon = matchDialog.findViewById<TextView>(R.id.tvMon)
        val tvTue = matchDialog.findViewById<TextView>(R.id.tvTue)
        val tvWed = matchDialog.findViewById<TextView>(R.id.tvWed)
        val tvThr = matchDialog.findViewById<TextView>(R.id.tvThr)
        val tvFri = matchDialog.findViewById<TextView>(R.id.tvFri)
        val tvSat = matchDialog.findViewById<TextView>(R.id.tvSat)

        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val currentDate = sdf.format(Date())
        tvFrom.text = currentDate
        tvTo.text = currentDate

        var  issunclick : Boolean = true
        var  ismonclick : Boolean = true
        var  istueclick : Boolean = true
        var  iswedclick : Boolean = true
        var  isthrclick : Boolean = true
        var  isfriclick : Boolean = true
        var  issatclick : Boolean = true

        val days : ArrayList<Int> = ArrayList()

        tvSun.setOnClickListener {
            if(issunclick){
                tvSun.setTextColor(resources.getColor(R.color.green))
                tvSun.background = resources.getDrawable(R.drawable.red_bg)
                days!!.add(0)
                issunclick = false
            }
            else{
                tvSun.setTextColor(resources.getColor(R.color.darkgreay))
                tvSun.background = resources.getDrawable(R.drawable.grey_border)
                days!!.remove(0)
                issunclick = true
            }
        }

        tvMon.setOnClickListener {
            if(ismonclick){
                tvMon.setTextColor(resources.getColor(R.color.green))
                tvMon.background = resources.getDrawable(R.drawable.green_bg)
                days!!.add(1)
                ismonclick = false
            }
            else{
                tvMon.setTextColor(resources.getColor(R.color.darkgreay))
                tvMon.background = resources.getDrawable(R.drawable.grey_border)
                days!!.remove(1)
                ismonclick = true
            }
        }

        tvTue.setOnClickListener {
            if(istueclick){
                tvTue.setTextColor(resources.getColor(R.color.green))
                tvTue.background = resources.getDrawable(R.drawable.green_bg)
                days!!.add(2)
                istueclick = false
            }
            else{
                tvTue.setTextColor(resources.getColor(R.color.darkgreay))
                tvTue.background = resources.getDrawable(R.drawable.grey_border)
                days!!.remove(2)
                istueclick = true
            }
        }

        tvWed.setOnClickListener {
            if(iswedclick){
                tvWed.setTextColor(resources.getColor(R.color.green))
                tvWed.background = resources.getDrawable(R.drawable.green_bg)
                days!!.add(3)
                iswedclick = false
            }
            else{
                tvWed.setTextColor(resources.getColor(R.color.darkgreay))
                tvWed.background = resources.getDrawable(R.drawable.grey_border)
                days!!.remove(3)
                iswedclick = true
            }
        }

        tvThr.setOnClickListener {
            if(isthrclick){
                tvThr.setTextColor(resources.getColor(R.color.green))
                tvThr.background = resources.getDrawable(R.drawable.green_bg)
                days!!.add(4)
                isthrclick = false
            }
            else{
                tvThr.setTextColor(resources.getColor(R.color.darkgreay))
                tvThr.background = resources.getDrawable(R.drawable.grey_border)
                days!!.remove(4)
                isthrclick = true
            }
        }

        tvFri.setOnClickListener {
            if(isfriclick){
                tvFri.setTextColor(resources.getColor(R.color.green))
                tvFri.background = resources.getDrawable(R.drawable.green_bg)
                days!!.add(5)
                isfriclick = false
            }
            else{
                tvFri.setTextColor(resources.getColor(R.color.darkgreay))
                tvFri.background = resources.getDrawable(R.drawable.grey_border)
                days!!.remove(5)
                isfriclick = true
            }
        }

        tvSat.setOnClickListener {
            if(issatclick){
                tvSat.setTextColor(resources.getColor(R.color.green))
                tvSat.background = resources.getDrawable(R.drawable.green_bg)
                days!!.add(6)
                issatclick = false
            }
            else{
                tvSat.setTextColor(resources.getColor(R.color.darkgreay))
                tvSat.background = resources.getDrawable(R.drawable.grey_border)
                days!!.remove(6)
                issatclick = true
            }
        }

        tvFrom.setOnClickListener { openCalender(tvFrom) }
        tvTo.setOnClickListener { openCalender(tvTo) }
        tvSave.setOnClickListener {
           // matchDialog.dismiss()
            if(tvFrom!!.text.toString().equals("")){
                Toast.makeText(this, "Please Select From Date", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
             if(tvTo!!.text.toString().equals("")){
                 Toast.makeText(this, "Please Select To Date", Toast.LENGTH_SHORT).show()
                 return@setOnClickListener
            }
             if(days!!.size == 0){
                 Toast.makeText(this, "Please Select Days", Toast.LENGTH_SHORT).show()
                 return@setOnClickListener
            }

            var localTime: Date? = null
            var localTimeTo: Date? = null
            try {
                localTime =
                    SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(
                        tvFrom.text.toString()
                    )
                localTimeTo =
                    SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(
                        tvTo.text.toString()
                    )
                Log.e("From", localTime!!.time.toString())
            } catch (e: ParseException) {
                e.printStackTrace()
                Log.e("exception",e.message!!)
            }


            Log.e("From", localTime!!.time.toString())
            Log.e("To", localTimeTo!!.time.toString())
            Log.e("days", days.toString())



            val createScheduleData = CreateScheduleData()
            createScheduleData.from = localTime.time
            createScheduleData.to = localTimeTo.time
            createScheduleData.days = days
            createSchedule(createScheduleData)
        }
        tvCancel.setOnClickListener { matchDialog.dismiss() }

        matchDialog.setCanceledOnTouchOutside(false)
        matchDialog.getWindow()!!
            .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        matchDialog.show()

    }

    fun openCalender(tvDate: TextView) {
        val newCalendar = Calendar.getInstance()
        val dateFormatter =
            SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
        val datePickerDialog = DatePickerDialog(
            this, R.style.DatePickerTheme,
            DatePickerDialog.OnDateSetListener { view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int ->
                val select_date: String
                val newDate = Calendar.getInstance()
                newDate[year, monthOfYear] = dayOfMonth
                select_date = dateFormatter.format(newDate.time) + ""
                tvDate.text = select_date
                var localTime: Date? = null
                try {
                    localTime =
                        SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(
                            select_date.toString()
                        )
                    Log.e("From", localTime!!.time.toString())
                } catch (e: ParseException) {
                    e.printStackTrace()
                    Log.e("exception",e.message!!)
                }

            }, newCalendar[Calendar.YEAR], newCalendar[Calendar.MONTH],
            newCalendar[Calendar.DAY_OF_MONTH]
        )
//        datePickerDialog.setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show()
    }

    fun openClock(tvDate: TextView) {
        val mcurrentTime = Calendar.getInstance()
        val hour = mcurrentTime[Calendar.HOUR_OF_DAY]
        val minute = mcurrentTime[Calendar.MINUTE]
        val mTimePicker: TimePickerDialog
        mTimePicker = TimePickerDialog(
            this, R.style.DatePickerTheme,
            TimePickerDialog.OnTimeSetListener { timePicker, selectedHour, selectedMinute ->
                tvDate.setText(
                    "$selectedHour:$selectedMinute"
                )
            },
            hour,
            minute,
            true
        ) //Yes 24 hour time

        mTimePicker.show()
    }

    //---------------------toolbar item click---------------------
//    override fun onLeftBtnClick(view: View) {
//        openDrawer()
//    }

    override fun onAppointmentClick(view: View) {
        closeDrawer()
    }

    override fun onLanguageClick(view: View) {
        closeDrawer()
        startActivity(Intent(this, LanguageActivity::class.java))
    }

    override fun onProfileClick(view: View) {
        closeDrawer()
    }

    override fun onSettintgsClick(view: View) {
        closeDrawer()
        startActivity(Intent(this, SettingActivity::class.java))
    }

    override fun onLogoutClick(view: View) {
        closeDrawer()
    }

    override fun onCloseClick(view: View) {
        closeDrawer()
    }

    fun createSchedule(createScheduleData: CreateScheduleData) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_login)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.show()
        val call: Call<CreateScheduleResponse?>? = Apis.aPIService.createSchedule(
            "Basic " + Utility.UserToken!!,
            Utility.loginId!!,
            createScheduleData
        )
        call!!.enqueue(object : Callback<CreateScheduleResponse?> {
            override fun onResponse(
                    call: Call<CreateScheduleResponse?>,
                    response: Response<CreateScheduleResponse?>
            ) {
                dialog.dismiss()
                val user: CreateScheduleResponse? = response.body()
                if (response.code() == 500) {
                    Toast.makeText(this@MainActivity,"From Date and To Date got conflict with existing records",Toast.LENGTH_SHORT).show()
                }
                else{
                    if (user!!.status!!.equals("true")) {
                        Log.e("success","response")
                        startActivity(Intent(this@MainActivity, MainActivity::class.java).
                        putExtra("value","createschedule"))
                    }
                }
            }

            override fun onFailure(
                    call: Call<CreateScheduleResponse?>,
                    t: Throwable
            ) {
                dialog.dismiss()
                Log.d("response", "gh" + t.message)
            }
        })
    }


}