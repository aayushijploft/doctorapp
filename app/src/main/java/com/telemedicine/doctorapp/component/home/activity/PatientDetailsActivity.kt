package com.telemedicine.doctorapp.component.home.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.tabs.TabLayout
import com.telemedicine.doctorapp.R
import com.telemedicine.doctorapp.component.drawer.activity.SettingActivity
import com.telemedicine.doctorapp.component.drawer.fragments.ChangePasswordFragment
import com.telemedicine.doctorapp.component.drawer.fragments.DigitalSignatureFragment
import com.telemedicine.doctorapp.component.home.fragments.patientdetails.PastAppointmentsFragment
import com.telemedicine.doctorapp.component.home.fragments.patientdetails.ProfileFragment
import com.telemedicine.doctorapp.component.home.viewmodel.PatientDetailsViewModel
import com.telemedicine.doctorapp.component.home.viewmodel.VisitInformationViewModel
import com.telemedicine.doctorapp.databinding.ActivityPatientDetailsBinding
import com.telemedicine.doctorapp.databinding.ActivityVisitInformationBinding

class PatientDetailsActivity : AppCompatActivity() {
    private lateinit var viewModel: PatientDetailsViewModel

    private var binding: ActivityPatientDetailsBinding? = null
    internal lateinit var viewpageradapter: ViewPagerAdapter //Declare PagerAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, PatientDetailsViewModel.Factory(this))
            .get(PatientDetailsViewModel::class.java)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_patient_details)

        binding!!.setLifecycleOwner(this)

        binding!!.viewModel = viewModel

        binding!!.ivBack.setOnClickListener { onBackPressed() }

        viewpageradapter=
            ViewPagerAdapter(supportFragmentManager)

        binding!!.viewPager.adapter=viewpageradapter  //Binding PagerAdapter with ViewPager
        binding!!.tabLayout.setupWithViewPager(binding!!.viewPager) //Binding ViewP

        binding!!.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                if(tab.position == 1){
                    binding!!.ivFilter.visibility = View.INVISIBLE
                }
                else binding!!.ivFilter.visibility = View.VISIBLE
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {

            }
            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

    }

    internal  class ViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        var fragment: Fragment? = null

        override fun getItem(position: Int): Fragment {
            if (position == 0) {
                fragment = PastAppointmentsFragment()
            } else if (position == 1) {
                fragment = ProfileFragment()
            }
            return fragment!!
        }

        override fun getCount(): Int {
            return 2
        }


        override fun getPageTitle(position: Int): CharSequence? {
            var title: String? = null
            if (position == 0) {
                title = "past appointment"
            } else if (position == 1) {
                title = "profile"
            }
            return title
        }
    }

}