package com.telemedicine.doctorapp.component.home.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.telemedicine.doctorapp.R
import com.telemedicine.doctorapp.component.home.viewmodel.VisitInformationViewModel
import com.telemedicine.doctorapp.databinding.ActivityVisitInformationBinding

class VisitInformationActivity : AppCompatActivity() {
    private lateinit var viewModel: VisitInformationViewModel

    private var binding: ActivityVisitInformationBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, VisitInformationViewModel.Factory(this))
                .get(VisitInformationViewModel::class.java)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_visit_information)

        binding!!.setLifecycleOwner(this)

        binding!!.viewModel = viewModel

        binding!!.ivBack.setOnClickListener { onBackPressed() }


    }
}