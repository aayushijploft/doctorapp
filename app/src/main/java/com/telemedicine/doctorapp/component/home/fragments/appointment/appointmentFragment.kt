package com.telemedicine.doctorapp.component.home.fragments.appointment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.ViewModelProvider
import com.telemedicine.doctorapp.R
import com.telemedicine.doctorapp.component.home.fragments.appointment.subfragments.past.PastFragment
import com.telemedicine.doctorapp.component.home.fragments.appointment.subfragments.pending.PendingFragment
import com.telemedicine.doctorapp.component.home.fragments.appointment.subfragments.upcoming.UpcomingFragment
import com.telemedicine.doctorapp.component.home.fragments.appointment.subfragments.waitingroom.WaitingRoomFragment
import com.telemedicine.doctorapp.component.home.viewmodel.MainViewModel
import com.telemedicine.doctorapp.databinding.FragmentAppointmentBinding


class appointmentFragment : Fragment() {

    private lateinit var viewModel: AppointmentViewModel
    internal lateinit var viewpageradapter:ViewPagerAdapter //Declare PagerAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        viewModel = ViewModelProvider(this, AppointmentViewModel.Factory(requireActivity()))
            .get(AppointmentViewModel::class.java)

        val binding: FragmentAppointmentBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_appointment, container, false)
        binding.viewModel = viewModel

        viewpageradapter= ViewPagerAdapter(requireActivity().supportFragmentManager)

        binding.viewPager.adapter=viewpageradapter  //Binding PagerAdapter with ViewPager
        binding.tabLayout.setupWithViewPager(binding.viewPager) //Binding ViewPager
        return binding.root
    }


    internal  class ViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment {
            var fragment: Fragment? = null
            if (position == 0) {
                fragment = PendingFragment()
            } else if (position == 1) {
                fragment = WaitingRoomFragment()
            } else if (position == 2) {
                fragment = UpcomingFragment()
            }
            else if (position == 3) {
                fragment = PastFragment()
            }
            return fragment!!
        }

        override fun getCount(): Int {
            return 4
        }

        override fun getPageTitle(position: Int): CharSequence? {
            var title: String? = null
            if (position == 0) {
                title = "PENDING"
            } else if (position == 1) {
                title = "Waiting Room"
            } else if (position == 2) {
                title = "Up Coming"
            }
            else if (position == 3) {
                title = "Past"
            }
            return title
        }
    }


}