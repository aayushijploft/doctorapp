package com.telemedicine.doctorapp.component.home.fragments.appointment.subfragments.past

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.telemedicine.doctorapp.R
import com.telemedicine.doctorapp.databinding.FragmentPastBinding

class PastFragment : Fragment() {

    private lateinit var viewModel: PastViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        viewModel = ViewModelProvider(this, PastViewModel.Factory(requireActivity()))
            .get(PastViewModel::class.java)
        val binding: FragmentPastBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_past, container, false)
        binding.viewModel = viewModel


        return binding.root

    }

}