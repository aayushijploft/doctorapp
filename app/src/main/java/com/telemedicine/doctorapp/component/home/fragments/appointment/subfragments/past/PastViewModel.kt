package com.telemedicine.doctorapp.component.home.fragments.appointment.subfragments.past

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class PastViewModel(val context: Context) : ViewModel(){


        class Factory(val context: Context) : ViewModelProvider.NewInstanceFactory() {

            override fun <T : ViewModel> create(modelClass: Class<T>): T {

                return PastViewModel(
                    context
                ) as T
            }
        }

}