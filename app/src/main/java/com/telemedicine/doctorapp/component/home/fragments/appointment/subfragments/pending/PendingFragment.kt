package com.telemedicine.doctorapp.component.home.fragments.appointment.subfragments.pending

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.telemedicine.doctorapp.R
import com.telemedicine.doctorapp.component.home.activity.VisitInformationActivity
import com.telemedicine.doctorapp.databinding.FragmentPendingBinding
import com.telemedicine.doctorapp.interfaces.ItemClickListenerExtraParam
import com.telemedicine.doctorapp.network.Apis
import com.telemedicine.doctorapp.pojo.login.LoginData
import com.telemedicine.doctorapp.pojo.login.LoginResponse
import com.telemedicine.doctorapp.utils.SessionManager
import com.telemedicine.doctorapp.utils.Utility
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class PendingFragment : Fragment() {

    private lateinit var viewModel: PendingViewModel
    private var pendingAdapter: PendingListAdapter? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        viewModel = ViewModelProvider(this, PendingViewModel.Factory(requireActivity()))
            .get(PendingViewModel::class.java)
        val binding: FragmentPendingBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_pending, container, false)
        binding.viewModel = viewModel

        binding.rvRecycleview.setHasFixedSize(true)
        binding.rvRecycleview.layoutManager = LinearLayoutManager(activity,
            LinearLayoutManager.VERTICAL,false)
        pendingAdapter =
            PendingListAdapter(requireActivity(), ItemClickListenerExtraParam { pos, value ->

                if(value.equals("confirm")){
                    showCustomToastDialog()
                }
                else if(value.equals("reject")){
                    showRejectDialog()
                }
                else if(value.equals("visitinfo")){
                    startActivity(Intent(activity, VisitInformationActivity::class.java))
                }
            }
            )
        binding.rvRecycleview.adapter = pendingAdapter


        return binding.root
    }


    internal class PendingListAdapter(val context: Context,
                                      val mItemClickListener: ItemClickListenerExtraParam
    ) :
        RecyclerView.Adapter<PendingListAdapter.MyViewHolder>() {
        internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

            var tvConfirm: TextView = view.findViewById(R.id.tvConfirm)
            var tvReject: TextView = view.findViewById(R.id.tvReject)
            var tvVisitiInfo: TextView = view.findViewById(R.id.tvVisitiInfo)
        }

        @NonNull
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(context)
                .inflate(R.layout.layout_pending_appointment_list, parent, false)
            return MyViewHolder(itemView)
        }

        @SuppressLint("UseCompatLoadingForDrawables", "SetTextI18n")
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

            holder.tvConfirm.setOnClickListener {
                mItemClickListener.itemClick(position,"confirm")
            }

            holder.tvReject.setOnClickListener {
                mItemClickListener.itemClick(position,"reject")
            }

            holder.tvVisitiInfo.setOnClickListener {
                mItemClickListener.itemClick(position,"visitinfo")
            }

        }

        override fun getItemCount(): Int {
            return 3
        }

    }

    fun showRejectDialog() {
        var matchDialog: Dialog? = null
        matchDialog = Dialog(requireActivity())
        matchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        matchDialog.setCancelable(false)
        matchDialog.setContentView(R.layout.dailog_delete_confirmation)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(matchDialog.getWindow()!!.getAttributes())
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        matchDialog.getWindow()!!.setAttributes(lp)
        val tvSave = matchDialog.findViewById<TextView>(R.id.tvSave)
        val tvCancel = matchDialog.findViewById<TextView>(R.id.tvCancel)
        val tvTitle = matchDialog.findViewById<TextView>(R.id.tvTitle)

        tvTitle.text = "Are you sure, you want to reject\nthis appointment"
        tvSave.text = "Reject"
        tvSave.setOnClickListener { matchDialog.dismiss() }
        tvCancel.setOnClickListener { matchDialog.dismiss() }

        matchDialog.setCanceledOnTouchOutside(false)
        matchDialog.getWindow()!!
            .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        matchDialog.show()

    }

    fun showCustomToastDialog() {
        val inflater = layoutInflater
        val layout: View = inflater.inflate(
            R.layout.customtoast,
            requireView().findViewById(R.id.toast_layout_root) as ViewGroup?
        )

//        val lp = WindowManager.LayoutParams()
//        lp.width = WindowManager.LayoutParams.MATCH_PARENT
//        lp.height = WindowManager.LayoutParams.MATCH_PARENT
//        layout.getWindow()!!.setAttributes(lp)

        val image: ImageView = layout.findViewById(R.id.image) as ImageView
        val text = layout.findViewById(R.id.text) as TextView
        text.text = "Hello! This is a custom toast!"

        val toast = Toast(context)
        toast.setGravity(Gravity.BOTTOM or Gravity.FILL_HORIZONTAL, 10, 0)
//        toast.setMargin(0.8f, 0f);
        toast.duration = Toast.LENGTH_LONG
        toast.setView(layout)
        toast.show()

    }


}