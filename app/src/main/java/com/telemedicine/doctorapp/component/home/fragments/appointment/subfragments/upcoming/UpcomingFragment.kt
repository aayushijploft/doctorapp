package com.telemedicine.doctorapp.component.home.fragments.appointment.subfragments.upcoming

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.telemedicine.doctorapp.R
import com.telemedicine.doctorapp.component.home.fragments.appointment.subfragments.pending.PendingViewModel
import com.telemedicine.doctorapp.databinding.FragmentUpcomingBinding


class UpcomingFragment : Fragment() {

    private lateinit var viewModel: UpcomingViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        viewModel = ViewModelProvider(this, UpcomingViewModel.Factory(requireActivity()))
            .get(UpcomingViewModel::class.java)
        val binding: FragmentUpcomingBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_upcoming, container, false)
        binding.viewModel = viewModel


        return binding.root
    }

}