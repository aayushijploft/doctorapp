package com.telemedicine.doctorapp.component.home.fragments.appointment.subfragments.upcoming

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class UpcomingViewModel(val context: Context) : ViewModel(){


    class Factory(val context: Context) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {

            return UpcomingViewModel(
                context
            ) as T
        }
    }

}