package com.telemedicine.doctorapp.component.home.fragments.appointment.subfragments.waitingroom

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.telemedicine.doctorapp.R
import com.telemedicine.doctorapp.component.home.activity.PatientDetailsActivity
import com.telemedicine.doctorapp.component.home.activity.VisitInformationActivity
import com.telemedicine.doctorapp.databinding.FragmentWaitingRoomBinding
import com.telemedicine.doctorapp.interfaces.ItemClickListenerExtraParam

class WaitingRoomFragment : Fragment() {
    private lateinit var viewModel: WaitingRoomViewModel
    private var waitingListAdapter: WaitingListAdapter? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this, WaitingRoomViewModel.Factory(requireActivity()))
            .get(WaitingRoomViewModel::class.java)
        val binding: FragmentWaitingRoomBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_waiting_room, container, false)
        binding.viewModel = viewModel
        binding.rvWaiting.setHasFixedSize(true)
        binding.rvWaiting.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL,false)
        waitingListAdapter = WaitingListAdapter(requireActivity()) { pos, value ->
            if (value.equals("patient_details")) {
                startActivity(Intent(activity, PatientDetailsActivity::class.java))
            } else if (value.equals("visitinfo")) {
                startActivity(Intent(activity, VisitInformationActivity::class.java))
            }
        }

        binding.rvWaiting.adapter = waitingListAdapter
        return binding.root
    }

    internal class WaitingListAdapter(val context: Context,
                                      val mItemClickListener: ItemClickListenerExtraParam
    ) :
        RecyclerView.Adapter<WaitingListAdapter.MyViewHolder>() {
        internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tvPatientDetails: TextView = view.findViewById(R.id.tvPatientDetails)
            var tvVisitiInfo: TextView = view.findViewById(R.id.tvVisitiInfo)
        }

        @NonNull
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(context)
                .inflate(R.layout.layout_waiting_list, parent, false)
            return MyViewHolder(itemView)
        }

        @SuppressLint("UseCompatLoadingForDrawables", "SetTextI18n")
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

            holder.tvPatientDetails.setOnClickListener {
                mItemClickListener.itemClick(position,"patient_details")
            }
            holder.tvVisitiInfo.setOnClickListener {
                mItemClickListener.itemClick(position,"visitinfo")
            }
        }

        override fun getItemCount(): Int {
            return 3
        }
    }

}