package com.telemedicine.doctorapp.component.home.fragments.appointment.subfragments.waitingroom

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class WaitingRoomViewModel(val context: Context) : ViewModel(){


    class Factory(val context: Context) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {

            return WaitingRoomViewModel(
                context
            ) as T
        }
    }

}