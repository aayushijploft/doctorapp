package com.telemedicine.doctorapp.component.home.fragments.createschedule

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.annotation.NonNull
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.telemedicine.doctorapp.R
import com.telemedicine.doctorapp.component.auth.activity.LoginActivity
import com.telemedicine.doctorapp.component.home.activity.MainActivity
import com.telemedicine.doctorapp.databinding.FragmentCreateScheduleBinding
import com.telemedicine.doctorapp.interfaces.ItemClickListenerExtraParam
import com.telemedicine.doctorapp.interfaces.ItemClickTextListener
import com.telemedicine.doctorapp.network.Apis
import com.telemedicine.doctorapp.pojo.createschedule.disabledates.DisableDataResponse
import com.telemedicine.doctorapp.pojo.createschedule.disabledates.DisableDatesData
import com.telemedicine.doctorapp.pojo.createschedule.schedules.*
import com.telemedicine.doctorapp.pojo.createschedule.slots.*
import com.telemedicine.doctorapp.pojo.spinner.SpinnerModel
import com.telemedicine.doctorapp.utils.Utility
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.temporal.ChronoUnit
import java.util.*
import java.util.stream.Collectors
import java.util.stream.Stream
import kotlin.collections.ArrayList


open class CreateScheduleFragment : Fragment() {

    private lateinit var viewModel: CreateScheduleViewModel
    private var createScheduleAdapter: CreateScheduleAdapter? = null
    private var binding: FragmentCreateScheduleBinding? = null
    var categoerylist: ArrayList<SpinnerModel> ? = null

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        viewModel = ViewModelProvider(this, CreateScheduleViewModel.Factory(requireActivity()))
            .get(CreateScheduleViewModel::class.java)

         binding = DataBindingUtil.inflate(
             inflater,
             R.layout.fragment_create_schedule, container, false
         )
        binding!!.viewModel = viewModel

        binding!!.tvCreate.setOnClickListener {
            showCreateScheduleDialog()
        }

        binding!!.layoutDeleteAll.setOnClickListener {
            showDeleteAllDialog()
        }

      //  binding!!.root.requestFocusFromTouch()

        binding!!.rvSchedule.setHasFixedSize(true)
        binding!!.rvSchedule.layoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.VERTICAL, false
        )

        binding!!.tvLogin.setOnClickListener {
            startActivity(Intent(requireActivity(), LoginActivity::class.java))
        }

        getScheduleList()
        binding!!.layoutSwipe.setOnRefreshListener {
            getScheduleList()
        }

        return binding!!.root
    }


//    ============START CREATE SCHEDULE===============
    private fun showCreateScheduleDialog() {
        var matchDialog: Dialog? = null
        matchDialog = Dialog(requireActivity())
        matchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        matchDialog.setCancelable(false)
        matchDialog.setContentView(R.layout.layout_create_schedule)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(matchDialog.getWindow()!!.getAttributes())
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        matchDialog.getWindow()!!.setAttributes(lp)

        val tvFrom = matchDialog.findViewById<TextView>(R.id.tvFrom)
        val tvTo = matchDialog.findViewById<TextView>(R.id.tvTo)
        val tvSave = matchDialog.findViewById<TextView>(R.id.tvSave)
        val tvCancel = matchDialog.findViewById<TextView>(R.id.tvCancel)
        val tvSun = matchDialog.findViewById<TextView>(R.id.tvSun)
        val tvMon = matchDialog.findViewById<TextView>(R.id.tvMon)
        val tvTue = matchDialog.findViewById<TextView>(R.id.tvTue)
        val tvWed = matchDialog.findViewById<TextView>(R.id.tvWed)
        val tvThr = matchDialog.findViewById<TextView>(R.id.tvThr)
        val tvFri = matchDialog.findViewById<TextView>(R.id.tvFri)
        val tvSat = matchDialog.findViewById<TextView>(R.id.tvSat)

    val sdf = SimpleDateFormat("dd/MM/yyyy")
    val currentDate = sdf.format(Date())
    tvFrom.text = currentDate
    tvTo.text = currentDate

        var  issunclick : Boolean = true
        var  ismonclick : Boolean = true
        var  istueclick : Boolean = true
        var  iswedclick : Boolean = true
        var  isthrclick : Boolean = true
        var  isfriclick : Boolean = true
        var  issatclick : Boolean = true

        val days : ArrayList<Int> = ArrayList()

        tvSun.setOnClickListener {
            if(issunclick){
                tvSun.setTextColor(resources.getColor(R.color.red1))
                tvSun.background = resources.getDrawable(R.drawable.red_bg)
                days!!.add(0)
                issunclick = false
            }
            else{
                tvSun.setTextColor(resources.getColor(R.color.darkgreay))
                tvSun.background = resources.getDrawable(R.drawable.grey_border)
                days!!.remove(0)
                issunclick = true
            }
        }

        tvMon.setOnClickListener {
            if(ismonclick){
                tvMon.setTextColor(resources.getColor(R.color.green))
                tvMon.background = resources.getDrawable(R.drawable.green_bg)
                days!!.add(1)
                ismonclick = false
            }
            else{
                tvMon.setTextColor(resources.getColor(R.color.darkgreay))
                tvMon.background = resources.getDrawable(R.drawable.grey_border)
                days!!.remove(1)
                ismonclick = true
            }
        }

        tvTue.setOnClickListener {
            if(istueclick){
                tvTue.setTextColor(resources.getColor(R.color.green))
                tvTue.background = resources.getDrawable(R.drawable.green_bg)
                days!!.add(2)
                istueclick = false
            }
            else{
                tvTue.setTextColor(resources.getColor(R.color.darkgreay))
                tvTue.background = resources.getDrawable(R.drawable.grey_border)
                days!!.remove(2)
                istueclick = true
            }
        }

        tvWed.setOnClickListener {
            if(iswedclick){
                tvWed.setTextColor(resources.getColor(R.color.green))
                tvWed.background = resources.getDrawable(R.drawable.green_bg)
                days!!.add(3)
                iswedclick = false
            }
            else{
                tvWed.setTextColor(resources.getColor(R.color.darkgreay))
                tvWed.background = resources.getDrawable(R.drawable.grey_border)
                days!!.remove(3)
                iswedclick = true
            }
        }

        tvThr.setOnClickListener {
            if(isthrclick){
                tvThr.setTextColor(resources.getColor(R.color.green))
                tvThr.background = resources.getDrawable(R.drawable.green_bg)
                days!!.add(4)
                isthrclick = false
            }
            else{
                tvThr.setTextColor(resources.getColor(R.color.darkgreay))
                tvThr.background = resources.getDrawable(R.drawable.grey_border)
                days!!.remove(4)
                isthrclick = true
            }
        }

        tvFri.setOnClickListener {
            if(isfriclick){
                tvFri.setTextColor(resources.getColor(R.color.green))
                tvFri.background = resources.getDrawable(R.drawable.green_bg)
                days!!.add(5)
                isfriclick = false
            }
            else{
                tvFri.setTextColor(resources.getColor(R.color.darkgreay))
                tvFri.background = resources.getDrawable(R.drawable.grey_border)
                days!!.remove(5)
                isfriclick = true
            }
        }

        tvSat.setOnClickListener {
            if(issatclick){
                tvSat.setTextColor(resources.getColor(R.color.green))
                tvSat.background = resources.getDrawable(R.drawable.green_bg)
                days!!.add(6)
                issatclick = false
            }
            else{
                tvSat.setTextColor(resources.getColor(R.color.darkgreay))
                tvSat.background = resources.getDrawable(R.drawable.grey_border)
                days!!.remove(6)
                issatclick = true
            }
        }

        tvFrom.setOnClickListener { openCalender(tvFrom) }
        tvTo.setOnClickListener { openCalender(tvTo) }

        tvSave.setOnClickListener {
            // matchDialog.dismiss()
            if(tvFrom!!.text.toString().equals("")){
                Toast.makeText(activity, "Please Select From Date", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(tvTo!!.text.toString().equals("")){
                Toast.makeText(activity, "Please Select To Date", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(days.size == 0){
                Toast.makeText(activity, "Please Select Days", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            var localTime: Date? = null
            var localTimeTo: Date? = null
            try {
                localTime =
                    SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(
                        tvFrom.text.toString()
                    )
                localTimeTo =
                    SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(
                        tvTo.text.toString()
                    )
                Log.e("From", localTime!!.time.toString())
            } catch (e: ParseException) {
                e.printStackTrace()
                Log.e("exception", e.message!!)
            }


            Log.e("From", localTime!!.time.toString())
            Log.e("To", localTimeTo!!.time.toString())
            Log.e("days", days.toString())



            val createScheduleData = CreateScheduleData()
            createScheduleData.from = localTime.time
            createScheduleData.to = localTimeTo.time
            createScheduleData.days = days
            createSchedule(createScheduleData, matchDialog)
        }

        tvCancel.setOnClickListener { matchDialog.dismiss() }

        matchDialog.setCanceledOnTouchOutside(false)
        matchDialog.getWindow()!!
            .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        matchDialog.show()
    }

    fun createSchedule(createScheduleData: CreateScheduleData, matchDialog: Dialog) {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_login)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.show()
        val call: Call<CreateScheduleResponse?>? = Apis.aPIService.createSchedule(
            "Basic " + Utility.UserToken!!,
            Utility.loginId!!,
            createScheduleData
        )
        call!!.enqueue(object : Callback<CreateScheduleResponse?> {
            override fun onResponse(
                call: Call<CreateScheduleResponse?>,
                response: Response<CreateScheduleResponse?>
            ) {
                dialog.dismiss()
                matchDialog.dismiss()
                val user: CreateScheduleResponse? = response.body()
                if (response.code() == 500) {
                    Toast.makeText(
                        activity,
                        "From Date and To Date got conflict with existing records",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    if (user!!.status!!.equals("true")) {
                        Log.e("success", "response")
                        getScheduleList()
                    }
                }


            }

            override fun onFailure(
                call: Call<CreateScheduleResponse?>,
                t: Throwable
            ) {
                dialog.dismiss()
                Log.d("response", "gh" + t.message)
            }
        })
    }
//    ============END CREATE SCHEDULE===============


//    ===========START CREATE SLOT==================
    fun showTimeSlotDialog(scheduleId: String) {
        var matchDialog: Dialog? = null
        matchDialog = Dialog(requireActivity())
        matchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        matchDialog.setCancelable(false)
        matchDialog.setContentView(R.layout.dailog_add_time_slot)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(matchDialog.getWindow()!!.getAttributes())
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        matchDialog.getWindow()!!.setAttributes(lp)
        val tvSave = matchDialog.findViewById<TextView>(R.id.tvSave)
        val tvCancel = matchDialog.findViewById<TextView>(R.id.tvCancel)
        val tvStart = matchDialog.findViewById<TextView>(R.id.tvStart)
        val tvEnd = matchDialog.findViewById<TextView>(R.id.tvEnd)
        val SpinnerDuration = matchDialog.findViewById<Spinner>(R.id.SpinnerDuration)
        val etLabel = matchDialog.findViewById<EditText>(R.id.etLabel)

        categoerylist = ArrayList()

        val spinnerModel = SpinnerModel()
        spinnerModel.tag = "15"
        spinnerModel.text = "Mins"
        categoerylist!!.add(spinnerModel)

        val spinnerModel1 = SpinnerModel()
        spinnerModel1.tag = "30"
        spinnerModel1.text = "Mins"
        categoerylist!!.add(spinnerModel1)

        val spinnerModel2 = SpinnerModel()
        spinnerModel2.tag = "45"
        spinnerModel2.text = "Mins"
        categoerylist!!.add(spinnerModel2)

        val spinnerModel3 = SpinnerModel()
        spinnerModel3.tag = "1"
        spinnerModel3.text = "Hour"
        categoerylist!!.add(spinnerModel3)

        Log.e("list", categoerylist!!.toString())

        val moodArrayAdapter = MoodArrayAdapter(requireContext(), categoerylist!!)
        SpinnerDuration.setAdapter(moodArrayAdapter)

        tvSave.setOnClickListener {
            if(tvStart!!.text.toString().equals("")){
                Toast.makeText(context, "Please Select From Time", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(tvEnd!!.text.toString().equals("")){
                Toast.makeText(context, "Please Select To Time", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(SpinnerDuration.selectedItem.equals("Select Duration")){
                Toast.makeText(context, "Please Select Time", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(etLabel!!.text.toString().equals("")){
                Toast.makeText(context, "Please Enter Label", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val timeStart = tvEnd!!.text.toString() //hh:mm

            val units = timeStart.split(":".toRegex()).toTypedArray() //will break the string up into an array

            val hour = units[0].toInt() //first element
            val minutes = units[1].toInt() //first element

            val Startduration = (hour * 60)+ minutes //add up our values


            val timeEnd = tvStart!!.text.toString() //hh:mm

            val units1 = timeEnd.split(":".toRegex()).toTypedArray() //will break the string up into an array

            val hour1 = units1[0].toInt() //first element
            val minutes1 = units1[1].toInt() //first element

            val Endduration = (hour1 * 60)+ minutes1 //add up our values
            val addSlotData = AddSlotData()
            addSlotData.from = Endduration.toLong()
            addSlotData.to = Startduration.toLong()

            addSlotData.duration= SpinnerDuration.selectedView.tag.toString().toInt()
            addSlotData.label= etLabel.text.toString()
            addSlotData.scheduleId= scheduleId
            createSlots(addSlotData, matchDialog)

        }

        tvCancel.setOnClickListener { matchDialog.dismiss() }

        tvStart.setOnClickListener { openClock(tvStart) }
        tvEnd.setOnClickListener { openClock(tvEnd) }




        matchDialog.setCanceledOnTouchOutside(false)
        matchDialog.getWindow()!!
            .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        matchDialog.show()

    }

    class MoodArrayAdapter(
        ctx: Context,
        moods: ArrayList<SpinnerModel>
    ) :
            ArrayAdapter<SpinnerModel>(ctx, 0, moods) {

        override fun getView(position: Int, recycledView: View?, parent: ViewGroup): View {
            return this.createView(position, recycledView, parent)
        }

        override fun getDropDownView(position: Int, recycledView: View?, parent: ViewGroup): View {
            return this.createView(position, recycledView, parent)
        }

        private fun createView(position: Int, recycledView: View?, parent: ViewGroup): View {

            val mood = getItem(position)

            val view = recycledView ?: LayoutInflater.from(context).inflate(
                R.layout.listitems_layout,
                parent,
                false
            )

            val names: TextView = view.findViewById(R.id.txtTitle)
            view.tag = mood!!.tag
            names.setText(mood.tag + " " + mood.text)

            return view
        }
    }

    fun createSlots(addSlotData: AddSlotData, dialog1: Dialog) {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_login)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.show()
        val call: Call<AddSlotResponse?>? = Apis.aPIService.createslotListbyScheduleID(
            "Basic " + Utility.UserToken!!,
            Utility.loginId!!,
            addSlotData
        )
        call!!.enqueue(object : Callback<AddSlotResponse?> {
            override fun onResponse(
                call: Call<AddSlotResponse?>,
                response: Response<AddSlotResponse?>
            ) {
                dialog.dismiss()
                val user: AddSlotResponse? = response.body()
                if (user!!.status!!.equals("true")) {
                    Log.e("success", "response")
                    dialog1.dismiss()
                    getScheduleList()
                }
            }

            override fun onFailure(
                call: Call<AddSlotResponse?>,
                t: Throwable
            ) {
                dialog.dismiss()
                Log.d("response", "gh" + t.message)
            }

        })

    }

    internal class SlotAdapter(
        val context: Context,
        val mlist: List<SlotListResponse.Data>,
        val mItemClickListener: ItemClickListenerExtraParam
    ) :
            RecyclerView.Adapter<SlotAdapter.MyViewHolder>() {
        internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

            var tvTime : TextView = view.findViewById(R.id.tvTime)
            var tvDuration : TextView = view.findViewById(R.id.tvDuration)
            var tvLable : TextView = view.findViewById(R.id.tvLable)
            var ivEdit : ImageView = view.findViewById(R.id.ivEdit)
            var ivDelete : ImageView = view.findViewById(R.id.ivDelete)
        }

        @NonNull
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(context)
                    .inflate(R.layout.layout_time_slot_list, parent, false)

            return MyViewHolder(itemView)
        }

        @SuppressLint("UseCompatLoadingForDrawables", "SetTextI18n")
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

            var FromTime = ConvertMinutesTimeToHHMMString(mlist.get(position).from!!.toInt())
            var ToTime = ConvertMinutesTimeToHHMMString(mlist.get(position).to!!.toInt())

            holder.tvTime.setText(FromTime + " - " + ToTime)
            if(mlist.get(position).duration.equals("1")){
                holder.tvDuration.setText("Duration: " + mlist.get(position).duration + " Hour")
            }
            else holder.tvDuration.setText("Duration: " + mlist.get(position).duration + " Mins")

            holder.tvLable.setText("Label: " + mlist.get(position).label)

            holder.ivEdit.setOnClickListener {
                mItemClickListener.itemClick(position, "editslot")
            }
            holder.ivDelete.setOnClickListener {
                mItemClickListener.itemClick(position, "deleteslot")
            }

        }

        fun ConvertMinutesTimeToHHMMString(minutesTime: Int): String? {
            val timeZone = TimeZone.getTimeZone("UTC")
            val df = SimpleDateFormat("hh:mm a")
            df.timeZone = timeZone
            return df.format(Date(minutesTime * 60 * 1000L))
        }

        override fun getItemCount(): Int {
            return mlist.size
        }

    }

    fun openCalender(tvDate: TextView) {
        val newCalendar = Calendar.getInstance()
        val dateFormatter =
            SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
        val datePickerDialog = DatePickerDialog(
            requireActivity(), R.style.DatePickerTheme,
            DatePickerDialog.OnDateSetListener { view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int ->
                val select_date: String
                val newDate = Calendar.getInstance()
                newDate[year, monthOfYear] = dayOfMonth
                select_date = dateFormatter.format(newDate.time) + ""
                tvDate.text = select_date
            }, newCalendar[Calendar.YEAR], newCalendar[Calendar.MONTH],
            newCalendar[Calendar.DAY_OF_MONTH]
        )
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show()
    }

    fun openClock(tvDate: TextView) {
        val mcurrentTime = Calendar.getInstance()
        val hour = mcurrentTime[Calendar.HOUR_OF_DAY]
        val minute = mcurrentTime[Calendar.MINUTE]
        val mTimePicker: TimePickerDialog
        mTimePicker = TimePickerDialog(
            context, R.style.DatePickerTheme,
            TimePickerDialog.OnTimeSetListener { timePicker, selectedHour, selectedMinute ->
                tvDate.setText(
                    "$selectedHour:$selectedMinute"
                )
            },
            hour,
            minute,
            false
        ) //Yes 24 hour time

//        mTimePicker.setTitle("Select Time")
        mTimePicker.show()
    }
//    ===========END CREATE SLOT==================

//    ============START GET SCHEDULE LIST=============================
    internal class CreateScheduleAdapter(
    val context: Context,
    val mlist: List<ScheduleListResponse.Data>,
    val mItemClickListener: ItemClickListenerExtraParam
) :
        RecyclerView.Adapter<CreateScheduleAdapter.MyViewHolder>() {
        internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

            var tvAddTimeSlots: TextView = view.findViewById(R.id.tvAddTimeSlots)
            var tvDates: TextView = view.findViewById(R.id.tvDates)
            var tvSun: TextView = view.findViewById(R.id.tvSun)
            var tvMon: TextView = view.findViewById(R.id.tvMon)
            var tvTue: TextView = view.findViewById(R.id.tvTue)
            var tvWed: TextView = view.findViewById(R.id.tvWed)
            var tvThr: TextView = view.findViewById(R.id.tvThr)
            var tvFri: TextView = view.findViewById(R.id.tvFri)
            var tvSat: TextView = view.findViewById(R.id.tvSat)
            var tvAddRemainingDays: TextView = view.findViewById(R.id.tvAddRemainingDays)
            var ivDisableDates: ImageView = view.findViewById(R.id.ivDisableDates)
            var ivDeleteSchedule: ImageView = view.findViewById(R.id.ivDeleteSchedule)
            var ivDelete: ImageView = view.findViewById(R.id.ivDelete)
            var ivEditDays: ImageView = view.findViewById(R.id.ivEditDays)
            var rvTimeSlots: RecyclerView = view.findViewById(R.id.rvTimeSlots)
        }

        @NonNull
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(context)
                .inflate(R.layout.layout_schedule_list, parent, false)

            return MyViewHolder(itemView)
        }

        @SuppressLint("UseCompatLoadingForDrawables", "SetTextI18n")
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.tvAddTimeSlots.setOnClickListener {
                mItemClickListener.itemClick(position, "addslots")
            }
            holder.ivDisableDates.setOnClickListener {
                mItemClickListener.itemClick(position, "disabledates")
            }
            holder.ivDelete.setOnClickListener {
                mItemClickListener.itemClick(position, "gruopdeleteschedule")
            }
            holder.ivDeleteSchedule.setOnClickListener {
                mItemClickListener.itemClick(position, "deleteschedule")
            }
            holder.ivEditDays.setOnClickListener {
                mItemClickListener.itemClick(position, "editdays")
            }
            holder.tvAddRemainingDays.setOnClickListener {
                mItemClickListener.itemClick(position, "remainingdays")
            }



            val timestampFrom = java.lang.Long.valueOf(mlist.get(position).from!!)
            var dateFrom = convertLongToTime(timestampFrom)

            val timestampTo = java.lang.Long.valueOf(mlist.get(position).to!!)
            var dateTo = convertLongToTime(timestampTo)
            holder.tvDates.setText(dateFrom + " - " + dateTo)

            for (i in 0 until mlist.get(position).days!!.size){
                if(mlist.get(position).days!!.get(i) == 0){
                    holder.tvSun.background = context.resources.getDrawable(R.drawable.green_bg)
                    holder.tvSun.setTextColor(context.getColor(R.color.green))
                    holder.tvSun.setTypeface(null, Typeface.BOLD)
                }
                else if(mlist.get(position).days!!.get(i) == 1){
                    holder.tvMon.background = context.resources.getDrawable(R.drawable.green_bg)
                    holder.tvMon.setTextColor(context.getColor(R.color.green))
                    holder.tvMon.setTypeface(null, Typeface.BOLD)
                }
                else if(mlist.get(position).days!!.get(i) == 2){
                    holder.tvTue.background = context.resources.getDrawable(R.drawable.green_bg)
                    holder.tvTue.setTextColor(context.getColor(R.color.green))
                    holder.tvTue.setTypeface(null, Typeface.BOLD)
                }
                else if(mlist.get(position).days!!.get(i) == 3){
                    holder.tvWed.background = context.resources.getDrawable(R.drawable.green_bg)
                    holder.tvWed.setTextColor(context.getColor(R.color.green))
                    holder.tvWed.setTypeface(null, Typeface.BOLD)
                }
                else if(mlist.get(position).days!!.get(i) == 4){
                    holder.tvThr.background = context.resources.getDrawable(R.drawable.green_bg)
                    holder.tvThr.setTextColor(context.getColor(R.color.green))
                    holder.tvThr.setTypeface(null, Typeface.BOLD)
                }
                else if(mlist.get(position).days!!.get(i) == 5){
                    holder.tvFri.background = context.resources.getDrawable(R.drawable.green_bg)
                    holder.tvFri.setTextColor(context.getColor(R.color.green))
                    holder.tvFri.setTypeface(null, Typeface.BOLD)
                }
                else if(mlist.get(position).days!!.get(i) == 6){
                    holder.tvSat.background = context.resources.getDrawable(R.drawable.green_bg)
                    holder.tvSat.setTextColor(context.getColor(R.color.green))
                    holder.tvSat.setTypeface(null, Typeface.BOLD)
                }
            }

            holder.rvTimeSlots.setHasFixedSize(true)
            holder.rvTimeSlots.layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )
            slotListbyScheduleID(
                holder.rvTimeSlots,
                mlist.get(position).scheduleId!!,
                holder.tvAddTimeSlots
            )


        }

        fun slotListbyScheduleID(
            recyclerView: RecyclerView,
            slotId: String,
            tvAddTimeSlots: TextView
        ) {
            val call: Call<SlotListResponse?>? = Apis.aPIService.slotListbyScheduleID(
                "Basic " + Utility.UserToken!!,
                Utility.loginId!!, slotId
            )
            call!!.enqueue(object : Callback<SlotListResponse?> {
                override fun onResponse(
                    call: Call<SlotListResponse?>,
                    response: Response<SlotListResponse?>
                ) {
//                    dialog.dismiss()
                    val user: SlotListResponse? = response.body()
                    if (user!!.status!!.equals("true")) {
                        if (user.data!!.size == 0) {
                            recyclerView.visibility = View.GONE
                            tvAddTimeSlots.setText("+ Add Time Slots")
                        } else {
                            recyclerView.visibility = View.VISIBLE
                            tvAddTimeSlots.setText("+ More Slots")
                            val slotAdapter = SlotAdapter(
                                context,
                                user.data!!, ItemClickListenerExtraParam { pos, value ->

                                    if (value.equals("editslot")) {
                                        showTimeSlotDialog(
                                            user.data!!.get(pos).scheduleId!!, user.data!!.get(
                                                pos
                                            ).slotId!!
                                        )
                                    }
                                    if (value.equals("deleteslot")) {
                                        showDeleteDialog(user.data!!.get(pos).slotId!!)
                                    }

                                })
                            recyclerView.adapter = slotAdapter
                        }
                    }

                }

                override fun onFailure(
                    call: Call<SlotListResponse?>,
                    t: Throwable
                ) {
//                    dialog.dismiss()
                    Log.d("response", "gh" + t.message)
                }
            })
        }
        fun showDeleteDialog(slotId: String) {
            var matchDialog: Dialog? = null
            matchDialog = Dialog(context)
            matchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            matchDialog.setCancelable(false)
            matchDialog.setContentView(R.layout.dailog_delete_confirmation)
            val lp = WindowManager.LayoutParams()
            lp.copyFrom(matchDialog.getWindow()!!.getAttributes())
            lp.width = WindowManager.LayoutParams.MATCH_PARENT
            lp.height = WindowManager.LayoutParams.MATCH_PARENT
            matchDialog.getWindow()!!.setAttributes(lp)
            val tvSave = matchDialog.findViewById<TextView>(R.id.tvSave)
            val tvCancel = matchDialog.findViewById<TextView>(R.id.tvCancel)
            val tvTitle = matchDialog.findViewById<TextView>(R.id.tvTitle)


            tvSave.setOnClickListener {
                deleteSlots(matchDialog, slotId)
            }
            tvCancel.setOnClickListener { matchDialog.dismiss() }

            matchDialog.setCanceledOnTouchOutside(false)
            matchDialog.getWindow()!!
                    .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            matchDialog.show()

        }

        fun deleteSlots(dialog1: Dialog, slotId: String) {
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.dialog_login)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            dialog.show()
            val call: Call<AddSlotResponse?>? = Apis.aPIService.deleteSlot(
                "Basic " + Utility.UserToken!!,
                Utility.loginId!!,
                slotId
            )
            call!!.enqueue(object : Callback<AddSlotResponse?> {
                override fun onResponse(
                    call: Call<AddSlotResponse?>,
                    response: Response<AddSlotResponse?>
                ) {
                    dialog.dismiss()

                    val user: AddSlotResponse? = response.body()
                    if (user!!.status!!.equals("true")) {
                        dialog1.dismiss()

                    }

                }

                override fun onFailure(
                    call: Call<AddSlotResponse?>,
                    t: Throwable
                ) {
                    dialog.dismiss()
                    dialog1.dismiss()
                    Log.d("response", "gh" + t.message)
                }
            })
        }


        fun showTimeSlotDialog(scheduleiD: String, slotId: String) {
            var matchDialog: Dialog? = null
            matchDialog = Dialog(context)
            matchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            matchDialog.setCancelable(false)
            matchDialog.setContentView(R.layout.dailog_add_time_slot)
            val lp = WindowManager.LayoutParams()
            lp.copyFrom(matchDialog.getWindow()!!.getAttributes())
            lp.width = WindowManager.LayoutParams.MATCH_PARENT
            lp.height = WindowManager.LayoutParams.MATCH_PARENT
            matchDialog.getWindow()!!.setAttributes(lp)

            val tvSave = matchDialog.findViewById<TextView>(R.id.tvSave)
            val tvCancel = matchDialog.findViewById<TextView>(R.id.tvCancel)
            val tvStart = matchDialog.findViewById<TextView>(R.id.tvStart)
            val tvEnd = matchDialog.findViewById<TextView>(R.id.tvEnd)
            val SpinnerDuration = matchDialog.findViewById<Spinner>(R.id.SpinnerDuration)
            val etLabel = matchDialog.findViewById<EditText>(R.id.etLabel)
            var categoerylist: ArrayList<SpinnerModel> ? = null
            categoerylist = ArrayList()

            val spinnerModel = SpinnerModel()
            spinnerModel.tag = "15"
            spinnerModel.text = "Mins"
            categoerylist.add(spinnerModel)

            val spinnerModel1 = SpinnerModel()
            spinnerModel1.tag = "30"
            spinnerModel1.text = "Mins"
            categoerylist.add(spinnerModel1)

            val spinnerModel2 = SpinnerModel()
            spinnerModel2.tag = "45"
            spinnerModel2.text = "Mins"
            categoerylist.add(spinnerModel2)

            val spinnerModel3 = SpinnerModel()
            spinnerModel3.tag = "1"
            spinnerModel3.text = "Hour"
            categoerylist.add(spinnerModel3)

            Log.e("list", categoerylist!!.toString())
            val moodArrayAdapter = MoodArrayAdapter(context, categoerylist!!)
            SpinnerDuration.setAdapter(moodArrayAdapter)

            getSlotData(slotId, tvStart, tvEnd, SpinnerDuration, etLabel)

            tvSave.setOnClickListener {
                val timeStart = tvEnd!!.text.toString() //hh:mm

                val units = timeStart.split(":".toRegex()).toTypedArray() //will break the string up into an array

                val hour = units[0].toInt() //first element
                val minutes = units[1].toInt()//first elemen
                Log.e("mmmm", minutes.toString())// t
                val passmin = minutes.toString().split(" ".toRegex()).toTypedArray()
                val Startduration = (hour * 60)+ passmin[0].toInt() //add up our values

                val timeEnd = tvStart!!.text.toString() //hh:mm

                val units1 = timeEnd.split(":".toRegex()).toTypedArray() //will break the string up into an array
                val hour1 = units1[0].toInt() //first element
                val minutes1 = units1[1].toInt().toString() //first element
                val passmin1 = minutes1.split(" ".toRegex()).toTypedArray()
                val Endduration = (hour1 * 60)+ passmin1[0].toInt() //add up our values


                val updateSlotData = UpdateSlotData()
                updateSlotData.from =  Endduration.toLong()
                updateSlotData.to =Startduration.toLong()
                updateSlotData.duration= SpinnerDuration.selectedView.tag.toString().toInt()
                updateSlotData.label= etLabel.text.toString()
                updateSlotData.scheduleId = scheduleiD
                updateSlots(updateSlotData, matchDialog, slotId)
            }

            tvCancel.setOnClickListener { matchDialog.dismiss() }
            tvStart.setOnClickListener { openClock(tvStart) }
            tvEnd.setOnClickListener { openClock(tvEnd) }
            matchDialog.setCanceledOnTouchOutside(false)
            matchDialog.getWindow()!!
                    .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            matchDialog.show()

        }

        fun openClock(tvDate: TextView) {
            val mcurrentTime = Calendar.getInstance()
            val hour = mcurrentTime[Calendar.HOUR_OF_DAY]
            val minute = mcurrentTime[Calendar.MINUTE]
            val mTimePicker: TimePickerDialog
            mTimePicker = TimePickerDialog(
                context, R.style.DatePickerTheme,
                TimePickerDialog.OnTimeSetListener { timePicker, selectedHour, selectedMinute ->
                    tvDate.setText(
                        "$selectedHour:$selectedMinute"
                    )
                },
                hour,
                minute,
                false
            )
            mTimePicker.show()
        }

        fun updateSlots(addSlotData: UpdateSlotData, dialog1: Dialog, slotId: String) {
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.dialog_login)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            dialog.show()

            val call: Call<UpdateSlotResponse?>? = Apis.aPIService.updateSlot(
                "Basic " + Utility.UserToken!!,
                Utility.loginId!!,
                slotId,
                addSlotData
            )

            call!!.enqueue(object : Callback<UpdateSlotResponse?> {
                override fun onResponse(
                    call: Call<UpdateSlotResponse?>,
                    response: Response<UpdateSlotResponse?>
                ) {
                    dialog.dismiss()

                    val user: UpdateSlotResponse? = response.body()
                    if (user!!.status!!.equals("true")) {
                        dialog1.dismiss()
                        context.startActivity(Intent(context,MainActivity::class.java).putExtra("value","createschedule"))
                    }

                }

                override fun onFailure(
                    call: Call<UpdateSlotResponse?>,
                    t: Throwable
                ) {
                    dialog.dismiss()
                    Log.d("response", "gh" + t.message)
                }
            })
        }


        fun getSlotData(
            slotId: String,
            tvStart: TextView,
            tvEnd: TextView,
            tvDuration: Spinner,
            etLable: EditText
        ) {
            val call: Call<SingleSlotResponse?>? = Apis.aPIService.getSlotDetails(
                "Basic " + Utility.UserToken!!, Utility.loginId!!,
                slotId
            )
            call!!.enqueue(object : Callback<SingleSlotResponse?> {
                override fun onResponse(
                    call: Call<SingleSlotResponse?>,
                    response: Response<SingleSlotResponse?>
                ) {
                    val user: SingleSlotResponse? = response.body()
                    if (user!!.status!!.equals("true")) {
                        var Start = ConvertMinutesTimeToHHMMString(user.data!!.from!!.toInt())
                        var End = ConvertMinutesTimeToHHMMString(user.data!!.to!!.toInt())
                        Log.d("response", "gh" + Start)
                        Log.d("response", "gh" + End)
                        tvStart.setText(Start)
                        tvEnd.setText(End)
                        etLable.setText(user.data!!.label!!)

                        if (user.data!!.duration!!.equals("15")) {
                            tvDuration.setSelection(0)
                        } else if (user.data!!.duration!!.equals("30")) {
                            tvDuration.setSelection(1)
                        } else if (user.data!!.duration!!.equals("45")) {
                            tvDuration.setSelection(2)
                        } else if (user.data!!.duration!!.equals("1")) {
                            tvDuration.setSelection(3)
                        }
                    }

                }

                override fun onFailure(
                    call: Call<SingleSlotResponse?>,
                    t: Throwable
                ) {
                    Log.d("response", "gh" + t.message)
                }
            })
        }


        fun ConvertMinutesTimeToHHMMString(minutesTime: Int): String? {
            val timeZone = TimeZone.getTimeZone("UTC")
            val df = SimpleDateFormat("hh:mm")
            df.timeZone = timeZone
            return df.format(Date(minutesTime * 60 * 1000L))
        }

        override fun getItemCount(): Int {
            return mlist.size
        }

        fun convertLongToTime(time: Long): String {
            val date = Date(time)
            val format = SimpleDateFormat("dd/MM/yyyy")
            return format.format(date)
        }
    }

    internal  fun getScheduleList() {
        val dialog = Dialog(requireActivity())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_login)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.show()
        val call: Call<ScheduleListResponse?>? = Apis.aPIService.getScheduleList(
            "Basic " + Utility.UserToken!!,
            Utility.loginId!!
        )
        call!!.enqueue(object : Callback<ScheduleListResponse?> {
            @RequiresApi(Build.VERSION_CODES.O)
            override fun onResponse(
                call: Call<ScheduleListResponse?>,
                response: Response<ScheduleListResponse?>
            ) {
                dialog.dismiss()
                binding!!.layoutSwipe.isRefreshing = false
                val user: ScheduleListResponse? = response.body()
                if (response.code() == 401) {
                    binding!!.layoutNoData.visibility = View.GONE
                    binding!!.rvSchedule.visibility = View.GONE
                    binding!!.layoutHead.visibility = View.GONE
                    binding!!.layoutExpired.visibility = View.VISIBLE
                } else {
                    if (user!!.status!!.equals("true")) {
                        if (user.data!!.size == 0) {
                            binding!!.layoutNoData.visibility = View.VISIBLE
                            binding!!.rvSchedule.visibility = View.GONE
                            binding!!.layoutExpired.visibility = View.GONE

                        } else {
                            binding!!.layoutNoData.visibility = View.GONE
                            binding!!.rvSchedule.visibility = View.VISIBLE
                            binding!!.layoutHead.visibility = View.VISIBLE
                            binding!!.layoutExpired.visibility = View.GONE
                            createScheduleAdapter = CreateScheduleAdapter(
                                requireActivity(),
                                user.data!!, ItemClickListenerExtraParam { pos, value ->
                                    if (value.equals("addslots")) {
                                        showTimeSlotDialog(user.data!!.get(pos).scheduleId!!)
                                    }
                                    if (value.equals("deleteschedule")) {
                                        showGroupDeleteDialog(
                                            user.data!!.get(pos).from!!, user.data!!.get(
                                                pos
                                            ).to!!
                                        )
                                    }
                                    if (value.equals("gruopdeleteschedule")) {
                                        showDeleteDialog(user.data!!.get(pos).scheduleId!!)
                                    }
                                    if (value.equals("editdays")) {

                                        showEditDaysDialog(
                                            user.data!!.get(pos).scheduleId!!,
                                            user.data!!.get(
                                                pos
                                            ).days!!,
                                            user.data!!.get(pos).from!!,
                                            user.data!!.get(pos).to!!,
                                            user.data!!.get(
                                                pos
                                            ).groupId!!
                                        )
                                    }
                                    if (value.equals("remainingdays")) {
                                        Log.e("click", "ccccccc")
                                        showRemainingDaysDialog(
                                            user.data!!.get(pos).scheduleId!!,
                                            user.data!!.get(
                                                pos
                                            ).days!!,
                                            user.data!!.get(pos).from!!,
                                            user.data!!.get(pos).to!!,
                                            user.data!!.get(
                                                pos
                                            ).groupId!!
                                        )
                                    }
                                    if (value.equals("disabledates")) {
                                        val timestampFrom =
                                            java.lang.Long.valueOf(user.data!!.get(pos).from!!)
                                        var dateFrom = convertLongToDate(timestampFrom)
                                        var monthFrom = convertLongToMonth(timestampFrom)
                                        var yearFrom = convertLongToYear(timestampFrom)
                                        val timestampTo =
                                            java.lang.Long.valueOf(user.data!!.get(pos).to!!)
                                        var dateTo = convertLongToDate(timestampTo)
                                        var monthTo = convertLongToMonth(timestampTo)
                                        var yearTo = convertLongToYear(timestampTo)
                                        val startDate = LocalDate.of(
                                            yearFrom.toInt(),
                                            monthFrom.toInt(),
                                            (dateFrom.toInt())
                                        )
                                        val endDate = LocalDate.of(
                                            yearTo.toInt(),
                                            monthTo.toInt(),
                                            (dateTo.toInt())
                                        )
                                        val numOfDays = ChronoUnit.DAYS.between(startDate, endDate)
                                        var daysRange: ArrayList<LocalDate>? = null
                                        daysRange = ArrayList()


                                        for (item in 0 until user.data!!.get(pos).days!!.size) {
                                            if (user.data!!.get(pos).days!!.get(item) == 0) {
                                                daysRange!!.addAll(
                                                    Stream.iterate(
                                                        startDate,
                                                        { date: LocalDate ->
                                                            date.plusDays(
                                                                1
                                                            )
                                                        }).limit(numOfDays).filter
                                                    { date: LocalDate -> date.dayOfWeek == DayOfWeek.MONDAY }
                                                        .collect(
                                                            Collectors.toList()
                                                        )
                                                )
                                            } else if (user.data!!.get(pos).days!!.get(item) == 1) {
                                                daysRange!!.addAll(
                                                    Stream.iterate(
                                                        startDate,
                                                        { date: LocalDate ->
                                                            date.plusDays(
                                                                1
                                                            )
                                                        }).limit(numOfDays).filter
                                                    { date: LocalDate -> date.dayOfWeek == DayOfWeek.MONDAY }
                                                        .collect(
                                                            Collectors.toList()
                                                        )
                                                )
                                            } else if (user.data!!.get(pos).days!!.get(item) == 2) {
                                                daysRange!!.addAll(
                                                    Stream.iterate(
                                                        startDate,
                                                        { date: LocalDate ->
                                                            date.plusDays(
                                                                1
                                                            )
                                                        }).limit(numOfDays).filter
                                                    { date: LocalDate -> date.dayOfWeek == DayOfWeek.TUESDAY }
                                                        .collect(
                                                            Collectors.toList()
                                                        )
                                                )
                                            } else if (user.data!!.get(pos).days!!.get(item) == 3) {
                                                daysRange!!.addAll(
                                                    Stream.iterate(
                                                        startDate,
                                                        { date: LocalDate ->
                                                            date.plusDays(
                                                                1
                                                            )
                                                        }).limit(numOfDays).filter
                                                    { date: LocalDate -> date.dayOfWeek == DayOfWeek.WEDNESDAY }
                                                        .collect(
                                                            Collectors.toList()
                                                        )
                                                )
                                            } else if (user.data!!.get(pos).days!!.get(item) == 4) {
                                                daysRange!!.addAll(
                                                    Stream.iterate(
                                                        startDate,
                                                        { date: LocalDate ->
                                                            date.plusDays(
                                                                1
                                                            )
                                                        }).limit(numOfDays).filter
                                                    { date: LocalDate -> date.dayOfWeek == DayOfWeek.THURSDAY }
                                                        .collect(
                                                            Collectors.toList()
                                                        )
                                                )
                                            } else if (user.data!!.get(pos).days!!.get(item) == 5) {
                                                daysRange!!.addAll(
                                                    Stream.iterate(
                                                        startDate,
                                                        { date: LocalDate ->
                                                            date.plusDays(
                                                                1
                                                            )
                                                        }).limit(numOfDays).filter
                                                    { date: LocalDate -> date.dayOfWeek == DayOfWeek.FRIDAY }
                                                        .collect(
                                                            Collectors.toList()
                                                        )
                                                )
                                            } else if (user.data!!.get(pos).days!!.get(item) == 6) {
                                                daysRange!!.addAll(
                                                    Stream.iterate(
                                                        startDate,
                                                        { date: LocalDate ->
                                                            date.plusDays(
                                                                1
                                                            )
                                                        }).limit(numOfDays).filter
                                                    { date: LocalDate -> date.dayOfWeek == DayOfWeek.SATURDAY }
                                                        .collect(
                                                            Collectors.toList()
                                                        )
                                                )
                                            }
                                        }
                                        Log.d("dfadsfdsfsdfsdf", daysRange.toString() + "__")
                                        val disabledateslist: ArrayList<DisableDatesModel> =
                                            ArrayList()

                                        for (item in 0 until daysRange.size) {
                                            val disableDatesModel = DisableDatesModel()
                                            var localTime: Date? = null
                                            try {
                                                localTime =
                                                    SimpleDateFormat(
                                                        "yyyy-mm-dd",
                                                        Locale.getDefault()
                                                    ).parse(
                                                        daysRange.get(item).toString()
                                                    )
                                                Log.e("From", localTime!!.time.toString())
                                                disableDatesModel.timestamp = localTime.time
                                                disableDatesModel.tag = ""
                                                disableDatesModel.date =
                                                    daysRange.get(item).toString()
                                                disableDatesModel.islectected = false
                                                disabledateslist.add(disableDatesModel)
                                                //  Collections.sort(disabledateslist, Collections.reverseOrder());
                                            } catch (e: ParseException) {
                                                e.printStackTrace()
                                                Log.e("exception", e.message!!)
                                            }
                                        }
                                        showDisableDatesDialog(disabledateslist)
                                    }
                                })
                            binding!!.rvSchedule.adapter = createScheduleAdapter
                        }
                    }
                }
            }

            override fun onFailure(
                call: Call<ScheduleListResponse?>,
                t: Throwable
            ) {
                dialog.dismiss()
                Log.d("response", "gh" + t.message)
            }
        })
    }
//    ============END GET SCHEDULE LIST===========================

//    ==================START DELETE SCHEDULE-GROUP-ALL=====================
    fun showDeleteDialog(scheduleId: String) {
        var matchDialog: Dialog? = null
        matchDialog = Dialog(requireActivity())
        matchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        matchDialog.setCancelable(false)
        matchDialog.setContentView(R.layout.dailog_delete_confirmation)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(matchDialog.getWindow()!!.getAttributes())
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        matchDialog.getWindow()!!.setAttributes(lp)
        val tvSave = matchDialog.findViewById<TextView>(R.id.tvSave)
        val tvCancel = matchDialog.findViewById<TextView>(R.id.tvCancel)

        tvSave.setOnClickListener {
            deleteSchedule(matchDialog, scheduleId)
        }

        val tvTitle = matchDialog.findViewById<TextView>(R.id.tvTitle)
        tvTitle.setText("Are you sure, you want to delete\nthis schedule ? ")
        tvCancel.setOnClickListener { matchDialog.dismiss() }

        matchDialog.setCanceledOnTouchOutside(false)
        matchDialog.getWindow()!!
                .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        matchDialog.show()
    }

    fun showGroupDeleteDialog(from: String, to: String) {
        var matchDialog: Dialog? = null
        matchDialog = Dialog(requireActivity())
        matchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        matchDialog.setCancelable(false)
        matchDialog.setContentView(R.layout.dailog_delete_confirmation)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(matchDialog.getWindow()!!.getAttributes())
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        matchDialog.getWindow()!!.setAttributes(lp)
        val tvSave = matchDialog.findViewById<TextView>(R.id.tvSave)
        val tvCancel = matchDialog.findViewById<TextView>(R.id.tvCancel)
        val tvTitle = matchDialog.findViewById<TextView>(R.id.tvTitle)
        tvTitle.setText("Are you sure, you want to delete\nGroup schedule ? ")
        tvSave.setOnClickListener {
            deleteGroupSchedule(matchDialog, from, to)
        }
        tvCancel.setOnClickListener { matchDialog.dismiss() }
        matchDialog.setCanceledOnTouchOutside(false)
        matchDialog.getWindow()!!
                .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        matchDialog.show()
    }

    fun showDeleteAllDialog() {
        var matchDialog: Dialog? = null
        matchDialog = Dialog(requireActivity())
        matchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        matchDialog.setCancelable(false)
        matchDialog.setContentView(R.layout.dailog_delete_confirmation)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(matchDialog.getWindow()!!.getAttributes())
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        matchDialog.getWindow()!!.setAttributes(lp)
        val tvSave = matchDialog.findViewById<TextView>(R.id.tvSave)
        val tvCancel = matchDialog.findViewById<TextView>(R.id.tvCancel)
        val tvTitle = matchDialog.findViewById<TextView>(R.id.tvTitle)
        tvTitle.setText("Are you sure, you want to delete\nall schedules ? ")
        tvSave.setOnClickListener {
            deleteAllSchedule(matchDialog)
        }
        tvCancel.setOnClickListener { matchDialog.dismiss() }
        matchDialog.setCanceledOnTouchOutside(false)
        matchDialog.getWindow()!!
                .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        matchDialog.show()

    }

    fun deleteSchedule(dialog1: Dialog, scheduleId: String) {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_login)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.show()
        val call: Call<DeleteScheduleResponse?>? = Apis.aPIService.deleteSchedule(
            "Basic " + Utility.UserToken!!,
            Utility.loginId!!,
            scheduleId
        )
        call!!.enqueue(object : Callback<DeleteScheduleResponse?> {
            override fun onResponse(
                call: Call<DeleteScheduleResponse?>,
                response: Response<DeleteScheduleResponse?>
            ) {
                dialog.dismiss()

                val user: DeleteScheduleResponse? = response.body()
                if (user!!.status!!.equals("true")) {
                    dialog1.dismiss()
                    getScheduleList()

                }
            }

            override fun onFailure(
                call: Call<DeleteScheduleResponse?>,
                t: Throwable
            ) {
                dialog.dismiss()
                dialog1.dismiss()
                Log.d("response", "gh" + t.message)
            }
        })
    }

    fun deleteGroupSchedule(dialog1: Dialog, from: String, to: String) {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_login)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.show()
        val call: Call<DeleteScheduleResponse?>? = Apis.aPIService.deletegroupschedule(
            "Basic " + Utility.UserToken!!,
            Utility.loginId!!,
            from, to
        )
        call!!.enqueue(object : Callback<DeleteScheduleResponse?> {
            override fun onResponse(
                call: Call<DeleteScheduleResponse?>,
                response: Response<DeleteScheduleResponse?>
            ) {
                dialog.dismiss()

                val user: DeleteScheduleResponse? = response.body()
                if (user!!.status!!.equals("true")) {
                    dialog1.dismiss()
                    getScheduleList()
                }
            }

            override fun onFailure(
                call: Call<DeleteScheduleResponse?>,
                t: Throwable
            ) {
                dialog.dismiss()
                dialog1.dismiss()
                Log.d("response", "gh" + t.message)
            }
        })
    }

    fun deleteAllSchedule(dialog1: Dialog) {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_login)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.show()
        val call: Call<DeleteScheduleResponse?>? = Apis.aPIService.deleteAllSchedule(
            "Basic " + Utility.UserToken!!,
            Utility.loginId!!
        )
        call!!.enqueue(object : Callback<DeleteScheduleResponse?> {
            override fun onResponse(
                call: Call<DeleteScheduleResponse?>,
                response: Response<DeleteScheduleResponse?>
            ) {
                dialog.dismiss()

                val user: DeleteScheduleResponse? = response.body()
                if (user!!.status!!.equals("true")) {
                    dialog1.dismiss()
                    getScheduleList()
                }
            }

            override fun onFailure(
                call: Call<DeleteScheduleResponse?>,
                t: Throwable
            ) {
                dialog.dismiss()
                dialog1.dismiss()
                Log.d("response", "gh" + t.message)
            }
        })
    }
//    ==================END DELETE SCHEDULE-GROUP-ALL=====================

//    ==================START DISABLE DATE FUNCTIONALITY=====================
    fun showDisableDatesDialog(daysRange: ArrayList<DisableDatesModel>) {
        var matchDialog: Dialog? = null
        matchDialog = Dialog(requireContext())
        matchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        matchDialog.setCancelable(false)
        matchDialog.setContentView(R.layout.dailog_disable_dates)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(matchDialog.getWindow()!!.getAttributes())
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        matchDialog.getWindow()!!.setAttributes(lp)
        val tvCancel = matchDialog.findViewById<TextView>(R.id.tvCancel)
        val rvDisableDates = matchDialog.findViewById<RecyclerView>(R.id.rvDisableDates)
        val tvSave = matchDialog.findViewById<TextView>(R.id.tvSave)
        rvDisableDates.setHasFixedSize(true)
    Collections.sort(daysRange,
        Comparator<DisableDatesModel?> { o1, o2 ->
            if (o1.date == null || o2.date == null) 0 else o2.date
                .compareTo(o1.date)
        })
        rvDisableDates.layoutManager = LinearLayoutManager(
            requireContext(),
            LinearLayoutManager.VERTICAL,
            true
        )
        val disableDatesAdapter = DisableDatesAdapter(
            requireContext(),
            daysRange,
            ItemClickTextListener { pos, textView ->
                if (daysRange.get(pos).islectected == true) {

                    daysRange.get(pos).islectected = false
                    textView.setTextColor(resources.getColor(R.color.black))
                    textView.setTypeface(null, Typeface.NORMAL);

                } else {
                    daysRange.get(pos).islectected = true
                    textView.setTextColor(resources.getColor(R.color.green))
                    textView.setTypeface(null, Typeface.BOLD);

                }
            })

        rvDisableDates.adapter = disableDatesAdapter
        tvCancel.setOnClickListener { matchDialog.dismiss() }
        tvSave.setOnClickListener {
            var list: ArrayList<Long> = ArrayList()
            for(item in 0 until daysRange.size){
                if(daysRange.get(item).islectected == true){

                    list.add(daysRange.get(item).timestamp)

                }
            }
            Log.e("datalist", list.toString())
            val disableDatesData = DisableDatesData()
            disableDatesData.dates = list
            createDisableDates(disableDatesData, matchDialog)
        }
        matchDialog.setCanceledOnTouchOutside(false)
        matchDialog.getWindow()!!
                .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        matchDialog.show()
    }

    internal class DisableDatesAdapter(
        val context: Context,
        val daysRange: ArrayList<DisableDatesModel>,
        val mItemClickListener: ItemClickTextListener
    ) :
            RecyclerView.Adapter<DisableDatesAdapter.MyViewHolder>() {
        internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var txtDisableDates : TextView = view.findViewById(R.id.txtDisableDates)
        }

        @NonNull
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(context)
                    .inflate(R.layout.disable_dates_item, parent, false)

            return MyViewHolder(itemView)
        }

        @SuppressLint("UseCompatLoadingForDrawables", "SetTextI18n")
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

           // val date = Date(daysRange.get(position).date)
            val formatter5 = SimpleDateFormat("dd/MM/yyyy")
           // val formats1 = formatter5.format(date)
            Log.e("dddd", daysRange.get(position).date);
            holder.txtDisableDates.setText(parseDateToddMMyyyy(daysRange.get(position).date))

            holder.txtDisableDates.setOnClickListener {
                mItemClickListener.itemClick(position, holder.txtDisableDates)
            }

        }

        override fun getItemCount(): Int {
            return daysRange.size
        }
        open fun parseDateToddMMyyyy(time: String?): String? {
            val inputPattern = "yyyy-MM-dd"
            val outputPattern = "dd/MM/yyyy"
            val inputFormat = SimpleDateFormat(inputPattern)
            val outputFormat = SimpleDateFormat(outputPattern)
            var date: Date? = null
            var str: String? = null
            try {
                date = inputFormat.parse(time)
                str = outputFormat.format(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return str
        }
    }

    fun createDisableDates(disableDatesData: DisableDatesData, matchDialog: Dialog) {
            val dialog = Dialog(requireContext())
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.dialog_login)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            dialog.show()
        val call: Call<DisableDataResponse?>? = Apis.aPIService.createDisableDates(
            "Basic " + Utility.UserToken!!, Utility.loginId!!,
            disableDatesData
        )
        call!!.enqueue(object : Callback<DisableDataResponse?> {
            override fun onResponse(
                call: Call<DisableDataResponse?>,
                response: Response<DisableDataResponse?>
            ) {
                dialog.dismiss()

                val user: DisableDataResponse? = response.body()
                if (user!!.status!!.equals("true")) {
                    matchDialog.dismiss()
                }

            }

            override fun onFailure(
                call: Call<DisableDataResponse?>,
                t: Throwable
            ) {
                dialog.dismiss()
                Log.d("response", "gh" + t.message)
            }
        })
    }


    class  DisableDatesModel{
        var date : String = ""
        var tag : String = ""
        var islectected : Boolean = false
        var timestamp : Long = 0
    }
//    ==================END DISABLE DATE FUNCTIONALITY=====================

//    ===========START DATE CONVERSION====================

    fun convertLongToDate(time: Long): String {
        val date = Date(time)
        val format = SimpleDateFormat("dd")
        return format.format(date)
    }

    fun convertLongToMonth(time: Long): String {
        val date = Date(time)
        val format = SimpleDateFormat("MM")
        return format.format(date)
    }

    fun convertLongToYear(time: Long): String {
        val date = Date(time)
        val format = SimpleDateFormat("yyyy")
        return format.format(date)
    }

//    ====================END DATE CONVERSION====================

//    ======================START UPDATE DAYS-SCHEDULE======================

    fun showEditDaysDialog(
        scheduleId: String,
        days: ArrayList<Int>,
        from: String,
        to: String,
        groupid: String
    ) {
        var matchDialog: Dialog? = null
        matchDialog = Dialog(requireActivity())
        matchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        matchDialog.setCancelable(false)
        matchDialog.setContentView(R.layout.layout_days)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(matchDialog.getWindow()!!.getAttributes())
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        matchDialog.getWindow()!!.setAttributes(lp)
        val tvSave = matchDialog.findViewById<TextView>(R.id.tvSave)
        val tvCancel = matchDialog.findViewById<TextView>(R.id.tvCancel)
        val tvTitle = matchDialog.findViewById<TextView>(R.id.tvTitle)
        val tvSun: TextView = matchDialog.findViewById(R.id.tvSun)
        val tvMon: TextView = matchDialog.findViewById(R.id.tvMon)
        val tvTue: TextView = matchDialog.findViewById(R.id.tvTue)
        val tvWed: TextView = matchDialog.findViewById(R.id.tvWed)
        val tvThr: TextView = matchDialog.findViewById(R.id.tvThr)
        val tvFri: TextView = matchDialog.findViewById(R.id.tvFri)
        val tvSat: TextView = matchDialog.findViewById(R.id.tvSat)
        var  issunclick : Boolean = true
        var  ismonclick : Boolean = true
        var  istueclick : Boolean = true
        var  iswedclick : Boolean = true
        var  isthrclick : Boolean = true
        var  isfriclick : Boolean = true
        var  issatclick : Boolean = true

        for (i in 0 until days.size){
            if(days.get(i) == 0){
                tvSun.background = requireContext().resources.getDrawable(R.drawable.green_bg)
                tvSun.setTextColor(requireContext().getColor(R.color.red))
                tvSun.setTypeface(null, Typeface.BOLD);
                issunclick = false
            }
            else if(days.get(i) == 1){
                tvMon.background = requireContext().resources.getDrawable(R.drawable.green_bg)
                tvMon.setTextColor(requireContext().getColor(R.color.green))
                tvMon.setTypeface(null, Typeface.BOLD);
                ismonclick = false
            }
            else if(days.get(i) == 2){
                tvTue.background = requireContext().resources.getDrawable(R.drawable.green_bg)
                tvTue.setTextColor(requireContext().getColor(R.color.green))
                tvTue.setTypeface(null, Typeface.BOLD);
                istueclick = false
            }
            else if(days.get(i) == 3){
                tvWed.background = requireContext().resources.getDrawable(R.drawable.green_bg)
              tvWed.setTextColor(requireContext().getColor(R.color.green))
                tvWed.setTypeface(null, Typeface.BOLD);
                iswedclick = false
            }
            else if(days.get(i) == 4){
               tvThr.background = requireContext().resources.getDrawable(R.drawable.green_bg)
               tvThr.setTextColor(requireContext().getColor(R.color.green))
                tvThr.setTypeface(null, Typeface.BOLD);
                isthrclick = false
            }
            else if(days.get(i) == 5){
                tvFri.background = requireContext().resources.getDrawable(R.drawable.green_bg)
               tvFri.setTextColor(requireContext().getColor(R.color.green))
                tvFri.setTypeface(null, Typeface.BOLD);
                isfriclick = false
            }
            else if(days.get(i) == 6){
                tvSat.background = requireContext().resources.getDrawable(R.drawable.green_bg)
                tvSat.setTextColor(requireContext().getColor(R.color.green))
                tvSat.setTypeface(null, Typeface.BOLD);
                issatclick = false
            }
        }

        var daysselect : ArrayList<Int> = ArrayList()
        daysselect = days

        tvSun.setOnClickListener {
            if(issunclick){
                tvSun.setTextColor(resources.getColor(R.color.red1))
                tvSun.background = resources.getDrawable(R.drawable.red_bg)
                tvSun.setTypeface(null, Typeface.BOLD);
                daysselect!!.add(0)
                issunclick = false
                Log.e("daysselect", daysselect.toString())
            }
            else{
                tvSun.setTextColor(resources.getColor(R.color.darkgreay))
                tvSun.background = resources.getDrawable(R.drawable.grey_border)
                daysselect!!.remove(0)
                issunclick = true
                Log.e("daysselect", daysselect.toString())
            }
        }

        tvMon.setOnClickListener {
            if(ismonclick){
                tvMon.setTextColor(resources.getColor(R.color.green))
                tvMon.background = resources.getDrawable(R.drawable.green_bg)
                tvMon.setTypeface(null, Typeface.BOLD);
                daysselect!!.add(1)
                ismonclick = false
                Log.e("daysselect", daysselect.toString())
            }

            else{
                tvMon.setTextColor(resources.getColor(R.color.darkgreay))
                tvMon.background = resources.getDrawable(R.drawable.grey_border)
                daysselect!!.remove(1)
                ismonclick = true
                Log.e("daysselect", daysselect.toString())
            }
        }

        tvTue.setOnClickListener {
            if(istueclick){
                tvTue.setTextColor(resources.getColor(R.color.green))
                tvTue.background = resources.getDrawable(R.drawable.green_bg)
                tvTue.setTypeface(null, Typeface.BOLD)
                daysselect!!.add(2)
                istueclick = false
                Log.e("daysselect", daysselect.toString())
            }
            else{
                tvTue.setTextColor(resources.getColor(R.color.darkgreay))
                tvTue.background = resources.getDrawable(R.drawable.grey_border)
                daysselect!!.remove(2)
                istueclick = true
                Log.e("daysselect", daysselect.toString())
            }
        }

        tvWed.setOnClickListener {
            if(iswedclick){
                tvWed.setTextColor(resources.getColor(R.color.green))
                tvWed.background = resources.getDrawable(R.drawable.green_bg)
                tvWed.setTypeface(null, Typeface.BOLD)
                daysselect!!.add(3)
                iswedclick = false
                Log.e("daysselect", daysselect.toString())
            }
            else{
                tvWed.setTextColor(resources.getColor(R.color.darkgreay))
                tvWed.background = resources.getDrawable(R.drawable.grey_border)

                daysselect!!.remove(3)
                iswedclick = true
                Log.e("daysselect", daysselect.toString())
            }
        }

        tvThr.setOnClickListener {
            if(isthrclick){
                tvThr.setTextColor(resources.getColor(R.color.green))
                tvThr.background = resources.getDrawable(R.drawable.green_bg)
                tvThr.setTypeface(null, Typeface.BOLD)
                daysselect!!.add(4)
                isthrclick = false
                Log.e("daysselect", daysselect.toString())
            }
            else{
                tvThr.setTextColor(resources.getColor(R.color.darkgreay))
                tvThr.background = resources.getDrawable(R.drawable.grey_border)
                daysselect!!.remove(4)
                isthrclick = true
                Log.e("daysselect", daysselect.toString())
            }
        }

        tvFri.setOnClickListener {
            if(isfriclick){
                tvFri.setTextColor(resources.getColor(R.color.green))
                tvFri.background = resources.getDrawable(R.drawable.green_bg)
                tvFri.setTypeface(null, Typeface.BOLD)
                daysselect!!.add(5)
                isfriclick = false
                Log.e("daysselect", daysselect.toString())
            }
            else{
                tvFri.setTextColor(resources.getColor(R.color.darkgreay))
                tvFri.background = resources.getDrawable(R.drawable.grey_border)
                daysselect!!.remove(5)
                isfriclick = true
                Log.e("daysselect", daysselect.toString())
            }
        }

        tvSat.setOnClickListener {
            if(issatclick){
                tvSat.setTextColor(resources.getColor(R.color.green))
                tvSat.background = resources.getDrawable(R.drawable.green_bg)
                tvSat.setTypeface(null, Typeface.BOLD)
                daysselect!!.add(6)
                issatclick = false
                Log.e("daysselect", daysselect.toString())
            }
            else{
                tvSat.setTextColor(resources.getColor(R.color.darkgreay))
                tvSat.background = resources.getDrawable(R.drawable.grey_border)
                daysselect!!.remove(6)
                issatclick = true
                Log.e("daysselect", daysselect.toString())
            }
        }

        tvSave.setOnClickListener {
            Log.e("daysselect", daysselect.toString())
            val updateCreateSchedule = UpdateCreateSchedule()
            updateCreateSchedule.from = from.toLong()
            updateCreateSchedule.to = to.toLong()
            updateCreateSchedule.groupId = groupid
            updateCreateSchedule.days = daysselect
            updateSchedule(updateCreateSchedule, scheduleId, matchDialog)
        }

        tvCancel.setOnClickListener { matchDialog.dismiss() }

        matchDialog.setCanceledOnTouchOutside(false)
        matchDialog.getWindow()!!
                .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        matchDialog.show()

    }

    fun showRemainingDaysDialog(
        scheduleId: String,
        days: ArrayList<Int>,
        from: String,
        to: String,
        groupid: String
    ) {
        var matchDialog: Dialog? = null
        matchDialog = Dialog(requireActivity())
        matchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        matchDialog.setCancelable(false)
        matchDialog.setContentView(R.layout.layout_days)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(matchDialog.getWindow()!!.getAttributes())
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        matchDialog.getWindow()!!.setAttributes(lp)
        val tvSave = matchDialog.findViewById<TextView>(R.id.tvSave)
        val tvCancel = matchDialog.findViewById<TextView>(R.id.tvCancel)
        val tvTitle = matchDialog.findViewById<TextView>(R.id.tvTitle)
        val tvSun: TextView = matchDialog.findViewById(R.id.tvSun)
        val tvMon: TextView = matchDialog.findViewById(R.id.tvMon)
        val tvTue: TextView = matchDialog.findViewById(R.id.tvTue)
        val tvWed: TextView = matchDialog.findViewById(R.id.tvWed)
        val tvThr: TextView = matchDialog.findViewById(R.id.tvThr)
        val tvFri: TextView = matchDialog.findViewById(R.id.tvFri)
        val tvSat: TextView = matchDialog.findViewById(R.id.tvSat)

        for (i in 0 until days.size){
            if(days.get(i) == 0){
                tvSun.background = requireContext().resources.getDrawable(R.drawable.disable_bg)
                tvSun.setTextColor(requireContext().getColor(R.color.green))
                tvSun.isEnabled = false
            }
            else if(days.get(i) == 1){
                tvMon.background = requireContext().resources.getDrawable(R.drawable.disable_bg)
                tvMon.setTextColor(requireContext().getColor(R.color.green))
                tvMon.isEnabled = false
            }
            else if(days.get(i) == 2){
                tvTue.background = requireContext().resources.getDrawable(R.drawable.disable_bg)
                tvTue.setTextColor(requireContext().getColor(R.color.green))
                tvTue.isEnabled = false
            }
            else if(days.get(i) == 3){
                tvWed.background = requireContext().resources.getDrawable(R.drawable.disable_bg)
              tvWed.setTextColor(requireContext().getColor(R.color.green))
                tvWed.isEnabled = false
            }
            else if(days.get(i) == 4){
               tvThr.background = requireContext().resources.getDrawable(R.drawable.disable_bg)
               tvThr.setTextColor(requireContext().getColor(R.color.green))
                tvThr.isEnabled = false
            }
            else if(days.get(i) == 5){
                tvFri.background = requireContext().resources.getDrawable(R.drawable.disable_bg)
               tvFri.setTextColor(requireContext().getColor(R.color.green))
                tvFri.isEnabled = false
            }
            else if(days.get(i) == 6){
                tvSat.background = requireContext().resources.getDrawable(R.drawable.disable_bg)
                tvSat.setTextColor(requireContext().getColor(R.color.green))
                tvSat.isEnabled = false
            }
        }

        var  issunclick : Boolean = true
        var  ismonclick : Boolean = true
        var  istueclick : Boolean = true
        var  iswedclick : Boolean = true
        var  isthrclick : Boolean = true
        var  isfriclick : Boolean = true
        var  issatclick : Boolean = true

        val daysselect : ArrayList<Int> = ArrayList()

//        daysselect = days

        tvSun.setOnClickListener {
            if(issunclick){
                tvSun.setTextColor(resources.getColor(R.color.red1))
                tvSun.background = resources.getDrawable(R.drawable.red_bg)
                daysselect.add(0)
                issunclick = false
            }
            else{
                tvSun.setTextColor(resources.getColor(R.color.darkgreay))
                tvSun.background = resources.getDrawable(R.drawable.grey_border)
                daysselect.remove(0)
                issunclick = true
            }
        }

        tvMon.setOnClickListener {
            if(ismonclick){
                tvMon.setTextColor(resources.getColor(R.color.green))
                tvMon.background = resources.getDrawable(R.drawable.green_bg)
                daysselect.add(1)
                ismonclick = false
            }
            else{
                tvMon.setTextColor(resources.getColor(R.color.darkgreay))
                tvMon.background = resources.getDrawable(R.drawable.grey_border)
                daysselect.remove(1)
                ismonclick = true
            }
        }

        tvTue.setOnClickListener {
            if(istueclick){
                tvTue.setTextColor(resources.getColor(R.color.green))
                tvTue.background = resources.getDrawable(R.drawable.green_bg)
                daysselect.add(2)
                istueclick = false
            }
            else{
                tvTue.setTextColor(resources.getColor(R.color.darkgreay))
                tvTue.background = resources.getDrawable(R.drawable.grey_border)
                daysselect.remove(2)
                istueclick = true
            }
        }

        tvWed.setOnClickListener {
            if(iswedclick){
                tvWed.setTextColor(resources.getColor(R.color.green))
                tvWed.background = resources.getDrawable(R.drawable.green_bg)
                daysselect.add(3)
                iswedclick = false
            }
            else{
                tvWed.setTextColor(resources.getColor(R.color.darkgreay))
                tvWed.background = resources.getDrawable(R.drawable.grey_border)
                daysselect.remove(3)
                iswedclick = true
            }
        }

        tvThr.setOnClickListener {
            if(isthrclick){
                tvThr.setTextColor(resources.getColor(R.color.green))
                tvThr.background = resources.getDrawable(R.drawable.green_bg)
                daysselect.add(4)
                isthrclick = false
            }
            else{
                tvThr.setTextColor(resources.getColor(R.color.darkgreay))
                tvThr.background = resources.getDrawable(R.drawable.grey_border)
                daysselect.remove(4)
                isthrclick = true
            }
        }

        tvFri.setOnClickListener {
            if(isfriclick){
                tvFri.setTextColor(resources.getColor(R.color.green))
                tvFri.background = resources.getDrawable(R.drawable.green_bg)
                daysselect.add(5)
                isfriclick = false
            }
            else{
                tvFri.setTextColor(resources.getColor(R.color.darkgreay))
                tvFri.background = resources.getDrawable(R.drawable.grey_border)
                daysselect.remove(5)
                isfriclick = true
            }
        }

        tvSat.setOnClickListener {
            if(issatclick){
                tvSat.setTextColor(resources.getColor(R.color.green))
                tvSat.background = resources.getDrawable(R.drawable.green_bg)
                daysselect.add(6)
                issatclick = false
            }
            else{
                tvSat.setTextColor(resources.getColor(R.color.darkgreay))
                tvSat.background = resources.getDrawable(R.drawable.grey_border)
                daysselect.remove(6)
                issatclick = true
            }
        }

        tvSave.setOnClickListener {
            val updateCreateSchedule = UpdateCreateSchedule()
            updateCreateSchedule.from = from.toLong()
            updateCreateSchedule.to = to.toLong()
            updateCreateSchedule.groupId = groupid
            updateCreateSchedule.days = daysselect
            createRemaingDaysSchedule(updateCreateSchedule, matchDialog)
        }

        tvCancel.setOnClickListener { matchDialog.dismiss()
            daysselect.clear()}
        matchDialog.setCanceledOnTouchOutside(false)
        matchDialog.getWindow()!!
                .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        matchDialog.show()

    }

    fun createRemaingDaysSchedule(createScheduleData: UpdateCreateSchedule, matchDialog: Dialog) {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_login)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.show()
        val call: Call<CreateScheduleResponse?>? = Apis.aPIService.createRemaingSchedule(
            "Basic " + Utility.UserToken!!,
            Utility.loginId!!,
            createScheduleData
        )
        call!!.enqueue(object : Callback<CreateScheduleResponse?> {
            override fun onResponse(
                call: Call<CreateScheduleResponse?>,
                response: Response<CreateScheduleResponse?>
            ) {
                dialog.dismiss()
                matchDialog.dismiss()
                val user: CreateScheduleResponse? = response.body()
                if (response.code() == 500) {

                } else {
                    if (user!!.status!!.equals("true")) {
                        Log.e("success", "response")
                        getScheduleList()
                    }
                }


            }

            override fun onFailure(
                call: Call<CreateScheduleResponse?>,
                t: Throwable
            ) {
                dialog.dismiss()
                Log.d("response", "gh" + t.message)
            }
        })
    }

    fun updateSchedule(
        createScheduleData: UpdateCreateSchedule,
        scheduleId: String,
        matchDialog: Dialog
    ) {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_login)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.show()
        val call: Call<UpdateScheduleResponse?>? = Apis.aPIService.updateSchedule(
            "Basic " + Utility.UserToken!!,
            Utility.loginId!!,
            scheduleId,
            createScheduleData
        )

        call!!.enqueue(object : Callback<UpdateScheduleResponse?> {
            override fun onResponse(
                call: Call<UpdateScheduleResponse?>,
                response: Response<UpdateScheduleResponse?>
            ) {
                dialog.dismiss()
                val user: UpdateScheduleResponse? = response.body()
                if (user!!.status!!.equals("true")) {
                    matchDialog.dismiss()
                    getScheduleList()
                }
            }

            override fun onFailure(
                call: Call<UpdateScheduleResponse?>,
                t: Throwable
            ) {
                dialog.dismiss()
                Log.d("response", "gh" + t.message)
            }
        })
    }

    override fun onResume() {
        super.onResume()
        Log.d("response", "gh" )
        getScheduleList()
    }

//    ======================END  UPDATE DAYS-SCHEDULE======================
}