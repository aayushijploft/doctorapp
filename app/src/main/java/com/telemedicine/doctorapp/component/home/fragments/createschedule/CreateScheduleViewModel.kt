package com.telemedicine.doctorapp.component.home.fragments.createschedule

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class CreateScheduleViewModel(val context: Context) : ViewModel(){
    class Factory(val context: Context) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {

            return CreateScheduleViewModel(
                context
            ) as T
        }
    }
}