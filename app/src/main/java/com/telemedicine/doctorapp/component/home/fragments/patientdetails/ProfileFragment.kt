package com.telemedicine.doctorapp.component.home.fragments.patientdetails

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.telemedicine.doctorapp.R
import com.telemedicine.doctorapp.component.home.fragments.appointment.AppointmentViewModel
import com.telemedicine.doctorapp.component.home.fragments.patientdetails.viewmodels.PastAppointmentsViewModel
import com.telemedicine.doctorapp.component.home.fragments.patientdetails.viewmodels.ProfileViewModel
import com.telemedicine.doctorapp.databinding.FragmentAppointmentBinding
import com.telemedicine.doctorapp.databinding.FragmentProfileBinding


class ProfileFragment : Fragment() {

    private lateinit var viewModel: ProfileViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        viewModel = ViewModelProvider(this, ProfileViewModel.Factory(requireActivity()))
            .get(ProfileViewModel::class.java)

        val binding: FragmentProfileBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_profile, container, false)
        binding.viewModel = viewModel

        return binding.root

    }

}