package com.telemedicine.doctorapp.component.home.fragments.patientdetails.viewmodels

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class PastAppointmentsViewModel(val context: Context) : ViewModel(){

    class Factory(val context: Context) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {

            return PastAppointmentsViewModel(
                context
            ) as T
        }
    }
}