package com.telemedicine.doctorapp.component.home.fragments.uploads

import android.Manifest
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Build.VERSION.SDK_INT
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.provider.Settings
import android.text.TextUtils
import android.text.format.DateFormat
import android.util.Log
import android.view.*
import android.webkit.WebView
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.asksira.bsimagepicker.BSImagePicker
import com.bumptech.glide.Glide
import com.telemedicine.doctorapp.R
import com.telemedicine.doctorapp.component.auth.activity.LoginActivity
import com.telemedicine.doctorapp.databinding.FragmentUploadBinding
import com.telemedicine.doctorapp.interfaces.ItemClickListenerExtraParam
import com.telemedicine.doctorapp.network.Apis
import com.telemedicine.doctorapp.network.Constant
import com.telemedicine.doctorapp.pojo.uploads.*
import com.telemedicine.doctorapp.utils.URIPathHelper
import com.telemedicine.doctorapp.utils.Utility
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.FilePickerConst
import droidninja.filepicker.models.sort.SortingTypes
import droidninja.filepicker.utils.ContentUriUtils.getFilePath
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.net.URISyntaxException
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


class UploadFragment : Fragment(), BSImagePicker.OnSingleImageSelectedListener, BSImagePicker.ImageLoaderDelegate {

    private lateinit var viewModel: UploadViewModel
    val PICK_IMAGE = 1
    val PICK_DOCUMENT = 2
    var multiBodyPart : MultipartBody.Part? = null
    private var binding: FragmentUploadBinding? = null
    private val RECORD_REQUEST_CODE = 101
    private val PERMISSION_REQUEST_CODE = 555
    private val MAX_ATTACHMENT_COUNT = 10
    private var docPaths: ArrayList<Uri> = ArrayList()
    private val photoPaths: ArrayList<Uri> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        viewModel = ViewModelProvider(this, UploadViewModel.Factory(requireActivity()))
                .get(UploadViewModel::class.java)

         binding= DataBindingUtil.inflate(
             inflater,
             R.layout.fragment_upload, container, false
         )
        binding!!.viewModel = viewModel

        binding!!.layoutUpload.setOnClickListener {
            showDocumentDialog()
        }
        if (SDK_INT >= Build.VERSION_CODES.R) {
            requestPermission()
        }
        else setupPermissions()

//
        binding!!.rvUpload.setHasFixedSize(true)
        binding!!.rvUpload.layoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )
        getUploadList()

        binding!!.tvLogin.setOnClickListener {
            startActivity(Intent(requireActivity(), LoginActivity::class.java))
        }

        binding!!.ivAdd.setOnClickListener {
            if(TextUtils.isEmpty(binding!!.etYoutubeUrl.text.toString())){
                Toast.makeText(context, "Please Enter Youtube URL", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val pattern = "^(http(s)?:\\/\\/)?((w){3}.)?youtu(be|.be)?(\\.com)?\\/.+"
            if (!binding!!.etYoutubeUrl.text.toString().isEmpty() && binding!!.etYoutubeUrl.text.toString().contains(
                    "youtu"
                )) {
                var id = extractYTId(binding!!.etYoutubeUrl.text.toString())
                Log.e("id", id!!)
                binding!!.etYoutubeUrl.setText("")

                val saveFileData = SaveYoutubeFileData()
                saveFileData.url = Constant.YOUTUBEURL+id
                saveFileData.uploadkey = id + ".youtube"
                saveFileData.uploadtype = "youtubeUrl"
                saveFileData.size = 0
                SaveYouTubeFile(saveFileData)

            } else {
                // Not Valid youtube URL
                Toast.makeText(context, "Enter valid youtube url", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }


        }
        return binding!!.root
    }
    fun onPickDoc() {
        val zips = arrayOf("zip", "rar")
        val pdfs = arrayOf("aac")
        val maxCount: Int = MAX_ATTACHMENT_COUNT - photoPaths.size
        if (docPaths.size + photoPaths.size === MAX_ATTACHMENT_COUNT) {
            Toast.makeText(
                requireActivity(),
                "Cannot select more than " + MAX_ATTACHMENT_COUNT.toString() + " items",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            FilePickerBuilder.instance
                .setMaxCount(1)
                .setSelectedFiles(docPaths)
                // .setActivityTheme(R.style.FilePickerTheme)
                .setActivityTitle("Please select doc")
                .setImageSizeLimit(5) //Provide Size in MB
                .setVideoSizeLimit(20) //                    .addFileSupport("ZIP", zips)
                //                    .addFileSupport("AAC", pdfs, R.drawable.pdf_blue)
                .enableDocSupport(true)
                .enableSelectAll(true)
                .sortDocumentsBy(SortingTypes.NAME)
                .withOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                .pickFile(this)
        }
    }
    private fun checkPermission() {
         if (SDK_INT >= Build.VERSION_CODES.R) {
            Environment.isExternalStorageManager()
        } else {
            val result =
                ContextCompat.checkSelfPermission(requireContext(), READ_EXTERNAL_STORAGE)
            val result1 =
                ContextCompat.checkSelfPermission(requireContext(), WRITE_EXTERNAL_STORAGE)
            result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED
        }
        requestPermission()
    }
    private fun requestPermission() {
        if (SDK_INT >= Build.VERSION_CODES.R) {
            try {
                val intent = Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION)
                intent.addCategory("android.intent.category.DEFAULT")
                intent.data =
                    Uri.parse(
                        String.format(
                            "package:%s", "com.telemedicine.doctorapp"
                        )
                    )
                startActivityForResult(intent, 2296)
            } catch (e: Exception) {
                val intent = Intent()
                intent.action = Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION
                startActivityForResult(intent, 2296)
            }
        } else {
            //below android 11
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(WRITE_EXTERNAL_STORAGE),
                PERMISSION_REQUEST_CODE
            )
        }
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0) {
                val READ_EXTERNAL_STORAGE = grantResults[0] == PackageManager.PERMISSION_GRANTED
                val WRITE_EXTERNAL_STORAGE = grantResults[1] == PackageManager.PERMISSION_GRANTED
                if (READ_EXTERNAL_STORAGE && WRITE_EXTERNAL_STORAGE) {
                    // perform action when allow permission success
                    setupPermissions()
                } else {
                    Toast.makeText(requireContext(), "Allow permission for storage access!", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }
    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(
            requireActivity(),
            Manifest.permission.CAMERA
        )
        val external = ContextCompat.checkSelfPermission(
            requireActivity(),
            Manifest.permission.READ_EXTERNAL_STORAGE
        )
        val internal = ContextCompat.checkSelfPermission(
            requireActivity(),
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )

         val manage = ContextCompat.checkSelfPermission(
             requireActivity(),
             Manifest.permission.MANAGE_EXTERNAL_STORAGE

         )
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.R) {
            Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION
        }

        if (permission != PackageManager.PERMISSION_GRANTED && external != PackageManager.PERMISSION_GRANTED
                && internal != PackageManager.PERMISSION_GRANTED  && manage != PackageManager.PERMISSION_GRANTED) {
            makeRequest()
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ),
            RECORD_REQUEST_CODE
        )
    }


    fun showDocumentDialog() {
        var matchDialog: Dialog? = null
        matchDialog = Dialog(requireActivity())
        matchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        matchDialog.setCancelable(false)
        matchDialog.setContentView(R.layout.dailog_picker)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(matchDialog.getWindow()!!.getAttributes())
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        matchDialog.getWindow()!!.setAttributes(lp)
        val tvSave = matchDialog.findViewById<TextView>(R.id.tvSave)
        val tvCancel = matchDialog.findViewById<TextView>(R.id.tvCancel)
        val tvTitle = matchDialog.findViewById<TextView>(R.id.tvTitle)

        tvCancel.setOnClickListener {
            matchDialog.dismiss()
            val intent = Intent()
            intent.setType("image/*")
            intent.setAction(Intent.ACTION_GET_CONTENT)
            startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE)
        }

        tvSave.setOnClickListener {
            matchDialog.dismiss()
            onPickDoc()
         //   val intent = Intent()
//            intent.setType("application/pdf")
//            intent.setAction(Intent.ACTION_GET_CONTENT)
//            startActivityForResult(Intent.createChooser(intent, "Select Document"), PICK_DOCUMENT)
        }
//        tvCancel.setOnClickListener { matchDialog.dismiss() }

        matchDialog.setCanceledOnTouchOutside(false)
        matchDialog.getWindow()!!
                .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        matchDialog.show()

    }

    fun pickImage() {
        val intent =
            Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"
        startActivityForResult(intent, PICK_IMAGE)
    }
    private fun getRealPathFromURI(contentURI: Uri): String? {
        val result: String?
        val cursor: Cursor = requireActivity().getContentResolver().query(
            contentURI,
            null,
            null,
            null,
            null
        )!!
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.path
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            result = cursor.getString(idx)
            cursor.close()
        }
        return result
    }
    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        if (resultCode == Activity.RESULT_OK && requestCode == PICK_IMAGE && null != data) {
            val imageData: Uri = data!!.data!!
          Log.e("image", imageData.toString())
            val uriPathHelper = URIPathHelper()
            val filePath = uriPathHelper.getPath(requireActivity(), data.data!!)
            //Log.e("filepath", filePath!!)
            val file = File(filePath!!)
            val file_size: Long = (file.length()).toString().toLong()
            val megabytes: Long = file_size / 1024
            val extension: String = file.getAbsolutePath().substring(
                file.getAbsolutePath().lastIndexOf(
                    "."
                )
            )
            val uploadtype = extension.split(".").toTypedArray()
            Log.e("megabytes", megabytes.toString())
            Log.e("uploadtype", uploadtype.get(1))
            Log.e("filesize", file_size.toString())
            Log.e("extension", extension)
            val requestFile: RequestBody =
                    RequestBody.create(MediaType.parse("image/jpg"), file)
            multiBodyPart =  MultipartBody.Part.createFormData(
                "fileData",
                file.getName(),
                requestFile
            )
            uploadFile(multiBodyPart!!, "image/" + uploadtype.get(1), file_size)
        }
        if (requestCode == 2296) {
            if (SDK_INT >= Build.VERSION_CODES.R) {
                if (Environment.isExternalStorageManager()) {
                    // perform action when allow permission success
                    setupPermissions()
                } else {
                    Toast.makeText(
                        requireContext(),
                        "Allow permission for storage access!",
                        Toast.LENGTH_SHORT
                    ).show();
                }
            }
        }
        if (requestCode == FilePickerConst.REQUEST_CODE_DOC && null != data) {
//            val imageData: Uri = data!!.data!!
            /// Log.e("document", imageData.toString())
            /// val uriPathHelper = URIPathHelper()
            // val filePath = uriPathHelper.getPath(requireActivity(), data.data!!)
            // val filePath = getPath(requireActivity(), data.data!!)
            // val PathHolder = data.data!!.path
            // var filePath: String? = ""
            // val Fpath: String = attr.data.getDataString()
            // filePath = getRealPathFromURI_API19(requireActivity(), Objects.requireNonNull(data.data)!!)
            /// filePath=imageData.toString()
            // val PathHolder = getRealPathFromURI_API19(requireActivity(), data.data!!.path)
//            getParcelableArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS)
            val dataList: ArrayList<Uri> = data.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_DOCS)!!
            if (dataList != null) {
                docPaths = ArrayList()
                docPaths.addAll(dataList)
            }
            //val path:String=""
            try {
                //make sure to use this getFilePath method from worker thread
                val path = getFilePath(requireContext(), docPaths.get(0))
                if (path != null) {
                   // Toast.makeText(requireContext(), path, Toast.LENGTH_SHORT).show()
                    // Log.e("filepath", filePath!!)
                    val file = File(path!!)
                    // val fileExt = MimeTypeMap.getFileExtensionFromUrl(path.toString())
                    //Log.e("megabyddddddtes", fileExt)
                    val file_size: Long = (file.length()).toString().toLong()
                    val megabytes: Long = file_size / 1024
                    val extension: String = file.getAbsolutePath().substring(
                        file.getAbsolutePath().lastIndexOf(
                            "."
                        )
                    )
                    val uploadtype = extension.split(".").toTypedArray()
                    Log.e("megabytes", megabytes.toString())
                    Log.e("uploadtype", uploadtype.get(1))
                    // Log.e("filesize", file_size.toString())
                    Log.e("extension", extension)
                    val requestFile: RequestBody =
                        RequestBody.create(MediaType.parse("*/*"), file)
                    multiBodyPart =  MultipartBody.Part.createFormData(
                        "fileData",
                        file.getName(),
                        requestFile
                    )
                    uploadFile(multiBodyPart!!, "application/" + uploadtype.get(1), file_size)
                }
            } catch (e: URISyntaxException) {
                e.printStackTrace()
            }


        }
    }

    override fun onSingleImageSelected(uri: Uri?, tag: String?) {

    }

    override fun loadImage(imageUri: Uri?, ivImage: ImageView?) {
       Log.e("uri", imageUri.toString())
    }

    fun getUploadList() {
        val dialog = Dialog(requireActivity())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_login)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.show()
        val call: Call<WaitingUploadResponse?>? = Apis.aPIService.getUploadList(
            "Basic " + Utility.UserToken!!, Utility.loginId!!
        )
        call!!.enqueue(object : Callback<WaitingUploadResponse?> {
            override fun onResponse(
                call: Call<WaitingUploadResponse?>,
                response: Response<WaitingUploadResponse?>
            ) {
                dialog.dismiss()
                val user: WaitingUploadResponse? = response.body()
                if (response.code() == 401) {
                    binding!!.layoutNoData.visibility = View.GONE
                    binding!!.rvUpload.visibility = View.GONE
                    binding!!.layoutExpired.visibility = View.VISIBLE
                } else {
                    if (user!!.status!!.equals("true")) {

                        Log.e("size", user.data!!.size.toString())
                        if (user.data!!.size == 0) {
                            binding!!.rvUpload.visibility = View.GONE
                            binding!!.layoutNoData.visibility = View.VISIBLE
                            binding!!.layoutExpired.visibility = View.GONE
                        } else {
                            binding!!.layoutNoData.visibility = View.GONE
                            binding!!.rvUpload.visibility = View.VISIBLE
                            binding!!.layoutExpired.visibility = View.GONE
                            val uploadAdapter = UploadAdapter(context!!, user.data!!,
                                ItemClickListenerExtraParam { pos, value ->
                                    if (value.equals("delete")) {
                                        showDeleteDialog(user.data!!.get(pos).uploadkey!!)
                                    }
                                    if (value.equals("open")) {
                                        showFileDialog(
                                            user.data!!.get(pos).url!!, user.data!!.get(
                                                pos
                                            ).uploadkey!!, user.data!!.get(pos).uploadtype!!
                                        )
                                    }
                                })
                            binding!!.rvUpload.adapter = uploadAdapter
                        }
                    }

                }
            }

            override fun onFailure(
                call: Call<WaitingUploadResponse?>,
                t: Throwable
            ) {
                dialog.dismiss()
                Log.d("response", "gh" + t.message)
            }
        })
    }

    internal class UploadAdapter(
        val context: Context,
        val mlist: List<WaitingUploadResponse.Data>,
        val mItemClickListener: ItemClickListenerExtraParam
    ) :
        RecyclerView.Adapter<UploadAdapter.MyViewHolder>() {
        internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tvName = view.findViewById<TextView>(R.id.tvName)
            var tvdate = view.findViewById<TextView>(R.id.tvdate)
            var tvSize = view.findViewById<TextView>(R.id.tvSize)
            var tvType = view.findViewById<TextView>(R.id.tvType)
            var ivDelete = view.findViewById<ImageView>(R.id.ivDelete)
            var layoutOpen = view.findViewById<LinearLayout>(R.id.layoutOpen)
        }

        @NonNull
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(context)
                .inflate(R.layout.item_waiting_upload, parent, false)
            return MyViewHolder(itemView)
        }

        @SuppressLint("UseCompatLoadingForDrawables", "SetTextI18n")
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.tvName.setText(mlist.get(position).uploadkey)
            holder.tvType.setText("Type: " + mlist.get(position).uploadtype)

            var totalsize: Long = mlist.get(position).size!!.toLong();
           // Log.d("qwerty", .toString())
            if(totalsize.toString().equals("0")){
                holder.tvSize.visibility = View.GONE
            }
            else{
                holder.tvSize.setText("Size: " + getFileSize(totalsize))
            }

            holder.tvdate.setText("Date: " + convertLongToTime(mlist.get(position).createdAt!!.toLong()))
            holder.ivDelete.setOnClickListener {
                mItemClickListener.itemClick(position, "delete")
            }
            holder.layoutOpen.setOnClickListener {
                mItemClickListener.itemClick(position, "open")
            }
        }

        override fun getItemCount(): Int {
            return mlist.size
        }
        fun getFileSize(size: Long): String? {
            if (size <= 0) return "0"
            val units = arrayOf("B", "KB", "MB", "GB", "TB")
            val digitGroups = (Math.log10(size.toDouble()) / Math.log10(1024.0)).toInt()
            return DecimalFormat("#,##0.#").format(size / Math.pow(1024.0, digitGroups.toDouble())).toString() + " " + units[digitGroups]
        }
        fun convertLongToTime(time: Long): String {
            val date = Date(time)
            val format = SimpleDateFormat("MMM dd, yyyy")
            return format.format(date)
        }
//        private fun getDate(time: Long): String? {
//            val cal: Calendar = Calendar.getInstance(Locale.ENGLISH)
//            cal.setTimeInMillis(time * 1000)
//            return DateFormat.format("dd-mm-yyyy", cal).toString()
//        }
        fun getDate(timestamp: Long) :String {
            val calendar = Calendar.getInstance(Locale.ENGLISH)
            calendar.timeInMillis = timestamp * 1000L
            val date = DateFormat.format("dd-MM-yyyy", calendar).toString()
            return date
        }
    }

    fun showDeleteDialog(uploadkey: String) {
        var matchDialog: Dialog? = null
        matchDialog = Dialog(requireActivity())
        matchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        matchDialog.setCancelable(false)
        matchDialog.setContentView(R.layout.dailog_delete_confirmation)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(matchDialog.getWindow()!!.getAttributes())
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        matchDialog.getWindow()!!.setAttributes(lp)
        val tvSave = matchDialog.findViewById<TextView>(R.id.tvSave)
        val tvCancel = matchDialog.findViewById<TextView>(R.id.tvCancel)
        val tvTitle = matchDialog.findViewById<TextView>(R.id.tvTitle)
        tvTitle.setText("Are you sure, you want to remove\nthis file ?")
        tvSave.setText("Yes")
        tvCancel.setText("No")

        tvSave.setOnClickListener {
           var deleteUploadData =  DeleteUploadData()
            deleteUploadData.uploadkey = uploadkey
            deleteUploads(deleteUploadData, matchDialog)
        }
        tvCancel.setOnClickListener { matchDialog.dismiss() }

        matchDialog.setCanceledOnTouchOutside(false)
        matchDialog.getWindow()!!
            .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        matchDialog.show()
    }

    fun deleteUploads(deleteUploadData: DeleteUploadData, matchDialog: Dialog) {
        val dialog = Dialog(requireActivity())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_login)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.show()
        val call: Call<DeleteUploadResponse?>? = Apis.aPIService.deleteUpload(
            "Basic " + Utility.UserToken!!, Utility.loginId!!, deleteUploadData
        )
        call!!.enqueue(object : Callback<DeleteUploadResponse?> {
            override fun onResponse(
                call: Call<DeleteUploadResponse?>,
                response: Response<DeleteUploadResponse?>
            ) {
                dialog.dismiss()
                val user: DeleteUploadResponse? = response.body()
                if (user!!.status!!.equals("true")) {
                    deleteFile(deleteUploadData)
                    matchDialog.dismiss()
                    getUploadList()
                }
            }

            override fun onFailure(
                call: Call<DeleteUploadResponse?>,
                t: Throwable
            ) {
                dialog.dismiss()
                Log.d("response", "gh" + t.message)
            }
        })
    }

    fun deleteFile(deleteUploadData: DeleteUploadData) {
        val dialog = Dialog(requireActivity())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_login)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.show()
        val call: Call<DeleteUploadResponse?>? = Apis.aPIService.deleteFile(
            "Basic " + Utility.UserToken!!, Utility.loginId!!, deleteUploadData
        )
        call!!.enqueue(object : Callback<DeleteUploadResponse?> {
            override fun onResponse(
                call: Call<DeleteUploadResponse?>,
                response: Response<DeleteUploadResponse?>
            ) {
                dialog.dismiss()
                val user: DeleteUploadResponse? = response.body()
                if (user!!.status!!.equals("true")) {

                }
            }

            override fun onFailure(
                call: Call<DeleteUploadResponse?>,
                t: Throwable
            ) {
                dialog.dismiss()
                Log.d("response", "gh" + t.message)
            }
        })
    }

    fun uploadFile(multipartBody: MultipartBody.Part, uploadtype: String, size: Long) {
        val dialog = Dialog(requireActivity())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_login)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.show()
        val call: Call<UploadFileResponse?>? = Apis.aPIService.updateFile(
            "Basic " + Utility.UserToken!!, Utility.loginId!!, multipartBody
        )
        call!!.enqueue(object : Callback<UploadFileResponse?> {
            override fun onResponse(
                call: Call<UploadFileResponse?>,
                response: Response<UploadFileResponse?>
            ) {
                dialog.dismiss()
                val user: UploadFileResponse? = response.body()
                if (user!!.status!!.equals("true")) {
                    val saveFileData = SaveFileData()
                    saveFileData.url = Constant.IMAGEURL + user.path
                    saveFileData.uploadkey = user.path!!
                    saveFileData.uploadtype = uploadtype
                    saveFileData.size = size
                    SaveFile(saveFileData)
                }
            }

            override fun onFailure(
                call: Call<UploadFileResponse?>,
                t: Throwable
            ) {
                dialog.dismiss()
                Log.d("response", "gh" + t.message)
            }
        })
    }
    fun SaveFile(saveFileData: SaveFileData) {
        val dialog = Dialog(requireActivity())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_login)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.show()
        val call: Call<UploadFileResponse?>? = Apis.aPIService.saveFile(
            "Basic " + Utility.UserToken!!, Utility.loginId!!, saveFileData
        )
        call!!.enqueue(object : Callback<UploadFileResponse?> {
            override fun onResponse(
                call: Call<UploadFileResponse?>,
                response: Response<UploadFileResponse?>
            ) {
                dialog.dismiss()
                val user: UploadFileResponse? = response.body()
                if (user!!.status!!.equals("true")) {
                    getUploadList()
                }
            }

            override fun onFailure(
                call: Call<UploadFileResponse?>,
                t: Throwable
            ) {
                dialog.dismiss()
                Log.d("response", "gh" + t.message)
            }
        })
    }

    fun SaveYouTubeFile(saveFileData: SaveYoutubeFileData) {
        val dialog = Dialog(requireActivity())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_login)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.show()
        val call: Call<UploadFileResponse?>? = Apis.aPIService.saveYoutubeFile(
            "Basic " + Utility.UserToken!!, Utility.loginId!!, saveFileData
        )
        call!!.enqueue(object : Callback<UploadFileResponse?> {
            override fun onResponse(
                call: Call<UploadFileResponse?>,
                response: Response<UploadFileResponse?>
            ) {
                dialog.dismiss()
                val user: UploadFileResponse? = response.body()
                if (user!!.status!!.equals("true")) {
                    getUploadList()
                }
            }

            override fun onFailure(
                call: Call<UploadFileResponse?>,
                t: Throwable
            ) {
                dialog.dismiss()
                Log.d("response", "gh" + t.message)
            }
        })
    }


    @SuppressLint("SetJavaScriptEnabled")
    fun showFileDialog(url: String, text: String, type: String) {
        var matchDialog: Dialog? = null
        matchDialog = Dialog(requireActivity())
        matchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        matchDialog.setCancelable(true)
        matchDialog.setContentView(R.layout.dailog_file)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(matchDialog.getWindow()!!.getAttributes())
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        matchDialog.getWindow()!!.setAttributes(lp)
        val ivImage = matchDialog.findViewById<ImageView>(R.id.ivImage)
        val tvTitle = matchDialog.findViewById<TextView>(R.id.tvTitle)
        val ivClose = matchDialog.findViewById<ImageView>(R.id.ivClose)
        val webview: WebView = matchDialog.findViewById(R.id.webview)
        webview.getSettings().setJavaScriptEnabled(true)


        Log.e("type", type)

        if(type.equals("application/pdf")){
            webview.visibility = View.VISIBLE
            ivImage.visibility = View.GONE
            webview.loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url=" + url)


        }
       else if(type.equals("youtubeUrl")){
            webview.visibility = View.VISIBLE
            ivImage.visibility = View.GONE
            webview.loadUrl(url)
           // webview.getSettings().safeBrowsingEnabled=true

        }
        else{
            webview.visibility = View.GONE
            ivImage.visibility = View.VISIBLE
            Glide.with(requireActivity())
                    .load(url)
                    .fitCenter()
                    .placeholder(R.drawable.loading)
                    .into(ivImage)
        }
        tvTitle.setText(text)
        ivClose.setOnClickListener {
            matchDialog.dismiss()
        }
;

        matchDialog.setCanceledOnTouchOutside(true)
        matchDialog.getWindow()!!
                .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        matchDialog.show()
    }

    fun extractYTId(videoUrl: String): String{
        var videoId = ""
        val regex =
            "http(?:s)?:\\/\\/(?:m.)?(?:www\\.)?youtu(?:\\.be\\/|be\\.com\\/(?:watch\\?(?:feature=youtu.be\\&)?v=|v\\/|embed\\/|user\\/(?:[\\w#]+\\/)+))([^&#?\\n]+)"
        val pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(videoUrl)
        if (matcher.find()) {
            videoId = matcher.group(1)
        }
        return videoId
    }

//    fun extractYTId(ytUrl: String?): String? {
//
//        var vId: String? = null
//        val pattern: Pattern = Pattern.compile(
//            "^https?://.*(?:youtu.be/|v/|u/\\w/|embed/|watch?v=)([^#&?]*).*$",
//            Pattern.CASE_INSENSITIVE
//        )
//        val matcher: Matcher = pattern.matcher(ytUrl)
//        if (matcher.matches()) {
//            vId = matcher.group(1)
//        }
//        return vId
//    }

}