package com.telemedicine.doctorapp.component.home.fragments.uploads

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class UploadViewModel(val context: Context) : ViewModel(){


    class Factory(val context: Context) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {

            return UploadViewModel(
                    context
            ) as T
        }
    }

}