package com.telemedicine.doctorapp.component.home.viewmodel

import android.content.Context
import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.telemedicine.doctorapp.component.home.activity.MainActivity

public class MainViewModel(val context: Context) : ViewModel(){




    class Factory(val context: Context) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {

            return MainViewModel(
                context
            ) as T
        }
    }

}