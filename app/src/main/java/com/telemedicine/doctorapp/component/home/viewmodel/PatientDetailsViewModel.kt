package com.telemedicine.doctorapp.component.home.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class PatientDetailsViewModel(val context: Context) : ViewModel(){

    class Factory(val context: Context) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {

            return PatientDetailsViewModel(
                context
            ) as T
        }
    }
}