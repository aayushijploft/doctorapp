package com.telemedicine.doctorapp.component.home.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class VisitInformationViewModel(val context: Context) : ViewModel(){

    class Factory(val context: Context) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {

            return VisitInformationViewModel(
                context
            ) as T
        }
    }
}