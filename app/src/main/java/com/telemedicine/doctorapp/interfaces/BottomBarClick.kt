package com.telemedicine.doctorapp.interfaces

import android.view.View

interface BottomBarClick {
    abstract fun onLeftBtnClick(view:View)
    abstract fun onRightBtnClick(view:View)
}