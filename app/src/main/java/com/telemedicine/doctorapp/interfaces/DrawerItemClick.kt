package com.telemedicine.doctorapp.interfaces

import android.view.View

interface DrawerItemClick {
    abstract fun onAppointmentClick(view: View)
    abstract fun onLanguageClick(view: View)
    abstract fun onProfileClick(view: View)
    abstract fun onSettintgsClick(view: View)
    abstract fun onLogoutClick(view: View)
    abstract fun onCloseClick(view: View)
}