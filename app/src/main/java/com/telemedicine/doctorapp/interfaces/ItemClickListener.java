package com.telemedicine.doctorapp.interfaces;

public interface ItemClickListener {
    void itemClick(int pos);
}
