package com.telemedicine.doctorapp.interfaces;

public interface ItemClickListenerExtraParam {
    void itemClick(int pos,String value);
}
