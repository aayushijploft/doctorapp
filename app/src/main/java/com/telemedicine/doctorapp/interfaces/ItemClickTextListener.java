package com.telemedicine.doctorapp.interfaces;

import android.widget.TextView;

public interface ItemClickTextListener {
    void itemClick(int pos, TextView textView);
}
