package  com.telemedicine.doctorapp.network



import com.telemedicine.doctorapp.pojo.createschedule.disabledates.DisableDataResponse
import com.telemedicine.doctorapp.pojo.createschedule.disabledates.DisableDatesData
import com.telemedicine.doctorapp.pojo.createschedule.schedules.*
import com.telemedicine.doctorapp.pojo.createschedule.slots.*
import com.telemedicine.doctorapp.pojo.login.LoginData
import com.telemedicine.doctorapp.pojo.login.LoginResponse
import com.telemedicine.doctorapp.pojo.uploads.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {
    @POST(Constant.LOGIN)
    fun login(@Body loginData: LoginData): Call<LoginResponse?>?

    @POST(Constant.CREATESCHEDULE)
    fun createSchedule(@Header("token") token: String,
                       @Header("sub") sub: String,
                       @Body createScheduleData: CreateScheduleData): Call<CreateScheduleResponse?>?

   @POST(Constant.CREATESCHEDULE)
    fun createRemaingSchedule(@Header("token") token: String,
                       @Header("sub") sub: String,
                       @Body createScheduleData: UpdateCreateSchedule): Call<CreateScheduleResponse?>?

    @PUT(Constant.CREATESCHEDULE)
    fun updateSchedule(@Header("token") token: String,
                       @Header("sub") sub: String,
                       @Query("id") id: String?,
                       @Body createScheduleData: UpdateCreateSchedule): Call<UpdateScheduleResponse?>?

    @GET(Constant.CREATESCHEDULE)
    fun getScheduleList(@Header("token") token: String,
                        @Header("sub") sub: String): Call<ScheduleListResponse?>

   @DELETE(Constant.CREATESCHEDULE)
    fun deletegroupschedule(@Header("token") token: String,
                        @Header("sub") sub: String,
                            @Query("from") from:String,
       @Query("to") to:String
   ): Call<DeleteScheduleResponse?>


     @POST(Constant.DELETESCHEDULE)
    fun deleteSchedule(@Header("token") token: String,
                        @Header("sub") sub: String,
     @Query("id") id:String): Call<DeleteScheduleResponse?>

    @DELETE(Constant.DELETEALLSCHEDULE)
    fun deleteAllSchedule(@Header("token") token: String,
                        @Header("sub") sub: String): Call<DeleteScheduleResponse?>

    @POST(Constant.SCHEDULESLOT)
    fun  createslotListbyScheduleID(@Header("token") token: String,
                              @Header("sub") sub: String,
            @Body addSlotData: AddSlotData): Call<AddSlotResponse?>?

    @PUT(Constant.SCHEDULESLOT)
    fun  updateSlot(@Header("token") token: String,
                              @Header("sub") sub: String,
                    @Query("id") id: String?,
            @Body addSlotData: UpdateSlotData): Call<UpdateSlotResponse?>?

    @DELETE(Constant.SCHEDULESLOT)
    fun  deleteSlot(@Header("token") token: String,
                              @Header("sub") sub: String,
                    @Query("id") id: String?, ): Call<AddSlotResponse?>?

    @GET(Constant.SCHEDULESLOT)
    fun  slotListbyScheduleID(@Header("token") token: String,
                              @Header("sub") sub: String,
            @Query("id") id: String?): Call<SlotListResponse?>?

  @GET(Constant.SCHEDULESLOT)
    fun  getSlotDetails(@Header("token") token: String,
                              @Header("sub") sub: String,
            @Query("slotId") slotId: String?): Call<SingleSlotResponse?>?

    @POST(Constant.CREATEDISABLEDATES)
    fun  createDisableDates(@Header("token") token: String,
                                    @Header("sub") sub: String,
                                    @Body disableDatesData: DisableDatesData): Call<DisableDataResponse?>?

    @GET(Constant.DOCTORUPLOADLIST)
    fun  getUploadList(@Header("token") token: String,
                        @Header("sub") sub: String, ): Call<WaitingUploadResponse?>?

    @POST(Constant.DELETEDOCTORUPLOADLIST)
    fun  deleteUpload(@Header("token") token: String,
                        @Header("sub") sub: String,
    @Body deleteUploadData: DeleteUploadData): Call<DeleteUploadResponse?>?

  @POST(Constant.DELETEFILE)
    fun  deleteFile(@Header("token") token: String,
                        @Header("sub") sub: String,
    @Body deleteUploadData: DeleteUploadData): Call<DeleteUploadResponse?>?

    @Multipart
    @POST(Constant.UPLOADDOCTOR)
    fun updateFile(
            @Header("token") token: String,
    @Header("sub") sub: String,
            @Part image : MultipartBody.Part,
    ): Call<UploadFileResponse?>?

 @POST(Constant.SAVEFILE)
    fun saveFile(
         @Header("token") token: String,
         @Header("sub") sub: String,
         @Body saveFileData: SaveFileData,
    ): Call<UploadFileResponse?>?


 @POST(Constant.SAVEFILE)
    fun saveYoutubeFile(
         @Header("token") token: String,
         @Header("sub") sub: String,
         @Body saveFileData: SaveYoutubeFileData,
    ): Call<UploadFileResponse?>?



}