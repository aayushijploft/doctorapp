package  com.telemedicine.doctorapp.network

object Constant {
    const val LOGIN = "users/login"
    const val CREATESCHEDULE = "schedule/schedule"
    const val SCHEDULESLOT = "schedule/slots"
    const val CREATEDISABLEDATES = "schedule/disabledates"
    const val DELETESCHEDULE = "schedule/schedule/deleteschedule"
    const val DELETEALLSCHEDULE = "schedule/schedule/deleteall"
    const val DOCTORUPLOADLIST = "users/doctoruploadlist"
    const val DELETEDOCTORUPLOADLIST = "users/deletewaitingroomupload"
    const val DELETEFILE = "api/deletewaitingroomfile"
    var IMAGEURL = "https://ayushon.instavc.com/api/api/uploads/doctorUploads/"
    var YOUTUBEURL = "https://www.youtube.com/embed/"
    const val UPLOADDOCTOR = "api/uploaddoctor"
    const val SAVEFILE = "schedule/waitingroomupload"

    val LOG_FILE_NAME: Any = "staffster.log"
}