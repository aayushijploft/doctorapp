package com.telemedicine.doctorapp.pojo.createschedule.disabledates

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.telemedicine.doctorapp.pojo.createschedule.slots.SingleSlotResponse

class DisableDataResponse {

    @Expose
    @SerializedName("status")
    var status: String? = null

    @Expose
    @SerializedName("data")
    var data: Data? = null

    class Data{
        @Expose
        @SerializedName("userId")
        var userId: String? = null

    }

}