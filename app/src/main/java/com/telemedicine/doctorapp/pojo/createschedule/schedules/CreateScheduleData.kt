package com.telemedicine.doctorapp.pojo.createschedule.schedules

import java.util.*

class CreateScheduleData {
    var from : Long = 0
    var to :  Long = 0
    var days: ArrayList<Int> ? = null
}