package com.telemedicine.doctorapp.pojo.createschedule.schedules

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CreateScheduleResponse {
    @Expose
    @SerializedName("status")
    var status: String? = null

    @Expose
    @SerializedName("data")
    var data: String? = null


}