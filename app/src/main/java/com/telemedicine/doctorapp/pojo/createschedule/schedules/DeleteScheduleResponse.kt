package com.telemedicine.doctorapp.pojo.createschedule.schedules

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DeleteScheduleResponse{
    @Expose
    @SerializedName("status")
    var status: String? = null
}