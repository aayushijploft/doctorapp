package com.telemedicine.doctorapp.pojo.createschedule.schedules

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class ScheduleListResponse {
    @Expose
    @SerializedName("data")
    public var data: List<Data>? =
        null

    @Expose
    @SerializedName("status")
    var status: String? = null

    @Expose
    @SerializedName("error")
    var error: String? = null

    class Data {

        @Expose
        @SerializedName("days")
        public var days: ArrayList<Int>? =
            null

        @Expose
        @SerializedName("createdAt")
        var createdAt: String? = null

        @Expose
        @SerializedName("groupId")
        var groupId: String? = null

        @Expose
        @SerializedName("from")
        var from: String? = null

        @Expose
        @SerializedName("to")
        var to: String? = null

        @Expose
        @SerializedName("userId")
        var userId: String? = null

        @Expose
        @SerializedName("scheduleId")
        var scheduleId: String? = null

    }

}