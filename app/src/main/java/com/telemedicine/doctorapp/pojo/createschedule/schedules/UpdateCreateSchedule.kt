package com.telemedicine.doctorapp.pojo.createschedule.schedules

import java.util.ArrayList

class UpdateCreateSchedule {
    var from : Long = 0
    var to :  Long = 0
    var groupId :  String = ""
    var days: ArrayList<Int>? = null
}