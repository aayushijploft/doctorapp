package com.telemedicine.doctorapp.pojo.createschedule.schedules

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UpdateScheduleResponse {
    @Expose
    @SerializedName("status")
    var status: String? = null

    @Expose
    @SerializedName("data")
    var data: Data? = null

    class Data{

    }

}