package com.telemedicine.doctorapp.pojo.createschedule.slots

class AddSlotData {

    var from : Long = 0
    var to : Long = 0
    var duration : Int = 0
    var label : String = ""
    var scheduleId : String = ""
}