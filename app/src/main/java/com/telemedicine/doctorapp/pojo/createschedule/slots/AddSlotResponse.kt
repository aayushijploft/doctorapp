package com.telemedicine.doctorapp.pojo.createschedule.slots

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AddSlotResponse {
    @Expose
    @SerializedName("status")
    var status: String? = null

    @Expose
    @SerializedName("data")
    var data: String? = null

}