package com.telemedicine.doctorapp.pojo.createschedule.slots

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SingleSlotResponse {

    @Expose
    @SerializedName("status")
    var status: String? = null

    @Expose
    @SerializedName("data")
    var data: Data? = null

    class Data{
        @Expose
        @SerializedName("duration")
        var duration: String? = null
        @Expose
        @SerializedName("createdAt")
        var createdAt: String? = null
        @Expose
        @SerializedName("slotId")
        var slotId: String? = null
        @Expose
        @SerializedName("from")
        var from: String? = null
        @Expose
        @SerializedName("to")
        var to: String? = null
        @Expose
        @SerializedName("label")
        var label: String? = null
        @Expose
        @SerializedName("userId")
        var userId: String? = null
        @Expose
        @SerializedName("scheduleId")
        var scheduleId: String? = null

    }
}