package com.telemedicine.doctorapp.pojo.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LoginResponse {
    @Expose
    @SerializedName("status")
    var status: String? = null

    @Expose
    @SerializedName("data")
    var data: Data? = null

    class Data {
        @Expose
        @SerializedName("userId")
        var userId: String? = null

        @Expose
        @SerializedName("userToken")
        var userToken: String? = null
    }

}