package com.telemedicine.doctorapp.pojo.uploads

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DeleteUploadResponse {
    @Expose
    @SerializedName("status")
    var status: String? = null
}