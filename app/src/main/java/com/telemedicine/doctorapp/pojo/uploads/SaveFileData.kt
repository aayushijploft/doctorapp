package com.telemedicine.doctorapp.pojo.uploads

class SaveFileData{
    var url : String = ""
    var uploadkey : String = ""
    var uploadtype : String = ""
    var size : Long = 0
}