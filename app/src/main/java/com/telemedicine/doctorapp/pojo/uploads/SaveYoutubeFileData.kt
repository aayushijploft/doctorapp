package com.telemedicine.doctorapp.pojo.uploads

class SaveYoutubeFileData {

    var url : String = ""
    var uploadkey : String = ""
    var uploadtype : String = ""
    var size : Long = 0
    var Uploadname :String = ""

}