package com.telemedicine.doctorapp.pojo.uploads

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UploadFileResponse {

    @Expose
    @SerializedName("status")
    var status: String? = null

    @Expose
    @SerializedName("path")
    var path: String? = null

}