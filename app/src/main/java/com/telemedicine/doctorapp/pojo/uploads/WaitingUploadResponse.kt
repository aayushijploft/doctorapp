package com.telemedicine.doctorapp.pojo.uploads

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class WaitingUploadResponse {

    @Expose
    @SerializedName("data")
    public var data: List<Data>? =
        null

    @Expose
    @SerializedName("status")
    var status: String? = null

    class Data{

        @Expose
        @SerializedName("createdAt")
        var createdAt: String? = null

        @Expose
        @SerializedName("uploadtype")
        var uploadtype: String? = null

        @Expose
        @SerializedName("size")
        var size: String? = "0"

        @Expose
        @SerializedName("doctorId")
        var doctorId: String? = null

        @Expose
        @SerializedName("uploadkey")
        var uploadkey: String? = null

        @Expose
        @SerializedName("url")
        var url: String? = null

    }


}