package com.jploft.dramaking.ui.base

import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.telemedicine.doctorapp.R
import com.telemedicine.doctorapp.interfaces.DrawerItemClick


abstract class BaseBindingActivity : AppCompatActivity(),
    View.OnClickListener{
    var mActivity: AppCompatActivity? = null

    // protected var sessionManager: SessionManager? = null
//    protected lateinit var appPreferences: AppPreferences
    protected var fragment: Fragment? = null
  //  protected abstract fun setBinding()
    protected abstract fun createActivityObject(savedInstanceState: Bundle?)
//    protected abstract fun initializeObject()
 //   protected abstract fun setListeners()
    private var isInternetChecked = false

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // sessionManager = SessionManager.Companion.getInstance(this)

//        appPreferences = AppPreferences.getPrefs(this)
        createActivityObject(savedInstanceState)
      //  setBinding()
       // initializeObject()
      //  setListeners()
//        networkChangeReceiver = NetworkChangeReceiver()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }



    override fun onPause() {
        super.onPause()
    }

    fun changeFragment(
        fragment: Fragment,
        isAddToBack: Boolean
    ) {
        this.fragment = fragment
        val transaction =
            this.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, fragment, fragment.javaClass.name)
        if (isAddToBack) transaction.addToBackStack(fragment.javaClass.name)
        transaction.commit()
    }

    open fun replaceFragment(fragment: Fragment, bundle: Bundle?) {
        val backStateName: String = fragment.javaClass.getName()
        val manager: FragmentManager = this.supportFragmentManager
        val fragmentPopped: Boolean = manager.popBackStackImmediate(backStateName, 0)
        if (!fragmentPopped && manager.findFragmentByTag(backStateName) == null) { //fragment not in back stack, create it.
            val ft: FragmentTransaction = manager.beginTransaction()
            fragment.arguments = bundle
            ft.replace(R.id.fragment_container, fragment, backStateName)
            ft.addToBackStack(backStateName)
            ft.commit()
        }
    }
    override fun onClick(view: View) {}

//    fun isCameraPermissionGiven(): Boolean {
//        try {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                if (checkSelfPermission(Manifest.permission.CAMERA)
//                    == PackageManager.PERMISSION_DENIED || checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED
//                    || checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || checkSelfPermission(
//                        Manifest.permission.RECORD_AUDIO
//                    ) == PackageManager.PERMISSION_DENIED
//                ) {
//                    requestPermissions(
//                        arrayOf(
//                            Manifest.permission.CAMERA,
//                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                            Manifest.permission.READ_EXTERNAL_STORAGE,
//                            Manifest.permission.RECORD_AUDIO
//                        ),
//                        Constants.PERMISSION_CAMERA_VIDEO_REQUEST_CODE
//                    )
//                    return false
//                } else {
//                    return true
//
//                }
//            } else {
//                return true
//            }
//        } catch (e: java.lang.Exception) {
//            e.printStackTrace()
//            return false
//        }
//    }

//    fun checkCameraVideoPermission() {
//
//        try {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                if (checkSelfPermission(Manifest.permission.CAMERA)
//                    == PackageManager.PERMISSION_DENIED || checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED
//                    || checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || checkSelfPermission(
//                        Manifest.permission.RECORD_AUDIO
//                    ) == PackageManager.PERMISSION_DENIED
//                ) {
//                    requestPermissions(
//                        arrayOf(
//                            Manifest.permission.CAMERA,
//                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                            Manifest.permission.READ_EXTERNAL_STORAGE,
//                            Manifest.permission.RECORD_AUDIO
//                        ),
//                        Constants.PERMISSION_CAMERA_VIDEO_REQUEST_CODE
//                    )
//                } else {
//                    val values = ContentValues()
//                    values.put(MediaStore.Images.Media.TITLE, "abcd.png")
//                    Config.CapturePhotoUri = contentResolver.insert(
//                        MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
//                        values
//                    )
//                    val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Config.CapturePhotoUri)
//                    startActivityForResult(cameraIntent, Constants.REQUEST_CAPTURE_VIDEO)
//
//                }
//            } else {
//                val values = ContentValues()
//                values.put(MediaStore.Images.Media.TITLE, "abcd.png")
//                Config.CapturePhotoUri = contentResolver.insert(
//                    MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
//                    values
//                )
//                val cameraIntent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Config.CapturePhotoUri)
//                startActivityForResult(cameraIntent, Constants.REQUEST_CAPTURE_VIDEO)
//
//            }
//        } catch (e: java.lang.Exception) {
//            e.printStackTrace()
//        }
//
//    }


}