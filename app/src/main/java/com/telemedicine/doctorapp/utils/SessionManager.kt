package com.telemedicine.doctorapp.utils

import android.content.Context
import android.content.SharedPreferences
import androidx.databinding.BaseObservable

class SessionManager : BaseObservable() {
    private val IS_LOGIN = "isLoggedIn"
    private val AUTH_TOKEN = "auth_token"
    private val SELECT_BALLOON = "select_balloon"
    private val SOCIAL = "social"
    private val USER_ID = "user_id"
    private val FULL_NAME = "full_name"
    private val PHONE = "phone"
    private val DEVICE_TOKEN = "device_token"
    private val ADDRESS = "address"
    private val BIO = "bio"
    private val PROFILE_IMAGE = "profile_image"
    private val LOCATION = "location"
    private val LATITUDE = "latitude"
    private val LONGITUDE = "longitude"
    private val SELECT_LOCATION = "select_location"
    private val FIREBASE_ID = "firebase_id"

    val isLogin: Boolean
        get() = shared!!.getBoolean(IS_LOGIN, false)

    fun setLogin() {
        editor!!.putBoolean(IS_LOGIN, true)
        editor!!.commit()
    }

    var selectBalloon: Boolean
        get() = shared!!.getBoolean(SELECT_BALLOON, false)
        set(selectBalloon) {
            editor!!.putBoolean(SELECT_BALLOON, selectBalloon)
            editor!!.commit()
        }

    var social: Boolean
        get() = shared!!.getBoolean(SOCIAL, false)
        set(social) {
            editor!!.putBoolean(SOCIAL, social)
            editor!!.commit()
        }
    var authToken: String?
        get() = shared!!.getString(AUTH_TOKEN, "")
        set(authToken) {
            editor!!.putString(AUTH_TOKEN, authToken)
            editor!!.commit()
        }

    fun getLOCATION(): String? {
        return shared!!.getString(LOCATION, "")
    }

    fun setLOCATION(location: String?) {
        editor!!.putString(LOCATION, location)
        editor!!.commit()
    }

    fun getLATITUDE(): String? {
        return shared!!.getString(LATITUDE, "")
    }

    fun setLATITUDE(latitude: String?) {
        editor!!.putString(LATITUDE, latitude)
        editor!!.commit()
    }

    fun getLONGITUDE(): String? {
        return shared!!.getString(LONGITUDE, "")
    }

    fun setLONGITUDE(longitude: String?) {
        editor!!.putString(LONGITUDE, longitude)
        editor!!.commit()
    }

    fun isSELECT_LOCATION(): Boolean {
        return shared!!.getBoolean(SELECT_LOCATION, false)
    }

    fun setSELECT_LOCATION() {
        editor!!.putBoolean(SELECT_LOCATION, true)
        editor!!.commit()
    }

    fun getUSER_ID(): String? {
        return shared!!.getString(USER_ID, "")
    }

    fun setUSER_ID(userId: String?) {
        editor!!.putString(USER_ID, userId)
        editor!!.commit()
    }

    fun getFULL_NAME(): String? {
        return shared!!.getString(FULL_NAME, "")
    }

    fun setFULL_NAME(fullName: String?) {
        editor!!.putString(FULL_NAME, fullName)
        editor!!.commit()
    }

    fun getPHONE(): String? {
        return shared!!.getString(PHONE, "")
    }

    fun setPHONE(phone: String?) {
        editor!!.putString(PHONE, phone)
        editor!!.commit()
    }

    fun getADDRESS(): String? {
        return shared!!.getString(ADDRESS, "")
    }

    fun setADDRESS(address: String?) {
        editor!!.putString(ADDRESS, address)
        editor!!.commit()
    }

    fun getBIO(): String? {
        return shared!!.getString(BIO, "")
    }

    fun setBIO(bio: String?) {
        editor!!.putString(BIO, bio)
        editor!!.commit()
    }

    fun getPROFILE_IMAGE(): String? {
        return shared!!.getString(PROFILE_IMAGE, "")
    }

    fun setPROFILE_IMAGE(profileImage: String?) {
        editor!!.putString(PROFILE_IMAGE, profileImage)
        editor!!.commit()
    }

    fun getFIREBASE_ID(): String? {
        return shared!!.getString(FIREBASE_ID, "")
    }

    fun setFIREBASE_ID(firebase_id: String?) {
        editor!!.putString(FIREBASE_ID, firebase_id)
        editor!!.commit()
    }

    fun getDEVICE_TOKEN(): String? {
        return shared!!.getString(DEVICE_TOKEN, "")
    }

    fun setDEVICE_TOKEN(deviceToken: String?) {
        editor!!.putString(DEVICE_TOKEN, deviceToken)
        editor!!.commit()
    }

    /*@Bindable("data")
    public UserData.Data getUserData() {

        UserData.Data userData = new UserData.Data();
        userData.setId(shared.getString(USER_ID, ""));
        userData.setFullname(shared.getString(FULL_NAME, ""));
        userData.setEmail(shared.getString(EMAIL, ""));
        userData.setPhone(shared.getString(PHONE, ""));
        userData.setStatus(shared.getString(STATUS, ""));
        userData.setDeviceId(shared.getString(DEVICE_ID, ""));
        userData.setVerifyStatus(shared.getString(VERIFY_STATUS, ""));
        userData.setFirstName(shared.getString(FIRST_NAME, ""));
        userData.setLastName(shared.getString(LAST_NAME, ""));
        userData.setDob(shared.getString(DOB, ""));
        userData.setGender(shared.getString(GENDER, ""));
        userData.setReferCode(shared.getString(REFER_CODE, ""));
        userData.setWallet(shared.getString(WALLET, ""));
        userData.setRewallet(shared.getString(REWALLET, ""));
        userData.setAddress(shared.getString(ADDRESS, ""));
        userData.setProfileImage(shared.getString(PROFILE_IMAGE, ""));
        userData.setPin(shared.getString(PIN, ""));
        userData.setUpi(shared.getString(UPI, ""));
        userData.setDelete(shared.getString(DELETE, ""));
        userData.setAddphone(shared.getString(ADD_PHONE, ""));
        return userData;
    }

    @Bindable("data")
    public void setUserData(UserData.Data userData) {

        editor.putString(USER_ID, userData.getId());
        editor.putString(FULL_NAME, userData.getFullname());
        editor.putString(EMAIL, userData.getEmail());

        editor.putString(PHONE, userData.getPhone());
        editor.putString(STATUS, userData.getStatus());
        editor.putString(DEVICE_ID, userData.getDeviceId());
        editor.putString(VERIFY_STATUS, userData.getVerifyStatus());
        editor.putString(FIRST_NAME, userData.getFirstName());
        editor.putString(LAST_NAME, userData.getLastName());
        editor.putString(DOB, userData.getDob());
        editor.putString(GENDER, userData.getGender());
        editor.putString(REFER_CODE, userData.getReferCode());
        editor.putString(WALLET, userData.getWallet());
        editor.putString(REWALLET, userData.getRewallet());
        editor.putString(ADDRESS, userData.getAddress());
        editor.putString(PROFILE_IMAGE, userData.getProfileImage());
        editor.putString(PIN, userData.getPin());
        editor.putString(UPI, userData.getUpi());
        editor.putString(DELETE, userData.getDelete());
        editor.putString(ADD_PHONE, userData.getAddphone());
        editor.commit();
    }*/
    fun logout() {
        editor!!.putString(USER_ID, "")
        editor!!.putString(FULL_NAME, "")
        editor!!.putString(PHONE, "")
        editor!!.putString(BIO, "")
        editor!!.putString(DEVICE_TOKEN, "")
        editor!!.putString(FIREBASE_ID, "")
        editor!!.putString(ADDRESS, "")
        editor!!.putString(PROFILE_IMAGE, "")
        editor!!.putString(LOCATION, "")
        editor!!.putBoolean(SELECT_LOCATION, false)
        editor!!.putString(LATITUDE, "")
        editor!!.putString(LONGITUDE, "")
        editor!!.putString(AUTH_TOKEN, "")
        editor!!.putBoolean(IS_LOGIN, false)
        editor!!.putBoolean(SELECT_BALLOON, false)
        editor!!.putBoolean(SOCIAL, false)
        editor!!.commit()
    }

    companion object {
        private var shared: SharedPreferences? = null
        private var editor: SharedPreferences.Editor? = null
        private var session: SessionManager? = null
       open fun getInstance(context: Context): SessionManager? {
            if (session == null) {
                session = SessionManager()
            }
            if (shared == null) {
                shared = context.getSharedPreferences("BalloonApp", Context.MODE_PRIVATE)
                editor = shared!!.edit()
            }
            return session
        }
    }
}