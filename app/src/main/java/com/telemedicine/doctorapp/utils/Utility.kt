package com.telemedicine.doctorapp.utils

import android.content.Context
import android.content.SharedPreferences
import com.telemedicine.doctorapp.App

object Utility {
    val pref: SharedPreferences
        get() = App.appContext!!
            .getSharedPreferences("DOCTORAPP", Context.MODE_PRIVATE)

    val isLogin: Boolean
        get() = pref.getBoolean("isalwaysLogin", false)

    fun setlogin() {
        val editor = pref.edit()
        editor.putBoolean("isalwaysLogin", true)
        editor.apply()
    }

    fun logout() {
        pref.edit().clear().apply()
    }

    fun setloginID(id: String?) {
        val editor = pref.edit()
        editor.putString("userId", id)
        editor.apply()
    }

    val loginId: String?
        get() = pref.getString("userId", "")

     fun setUserToken(id: String?) {
        val editor = pref.edit()
        editor.putString("userToken", id)
        editor.apply()
    }

    val UserToken: String?
        get() = pref.getString("userToken", "")

}